<!-- INTRO STEWD -->

<div class="intro">
	<img src="../stewed-0.1.1/assents/img/background.jpg" alt="">
</div>
<div class="intro-info">
	<h1> Stewed <span>| Cook Your Dinner! </span></h1>
	<p>This is a responsive framework: CSS, HTML & JavaScript.</p>
	<div class="space-30"></div>
	<button class="oi"><i class="icon-dot-1"></i> Download <i class="icon-dot-1"></i></button>
</div>

<div class="functions">
	<div class="grid-11">
		<div class="large-33"> 
			<div class="content-icons">
				<i class="icon-cog-alt"></i> 
			</div>
			<div class="content-text">
				<h4>Easy and Fast</h4>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi possimus, sequi, eligendi, culpa quod provident vero dolorum tempora, dignissimos consequatur laudantium perferendis!</p>
			</div>
		</div>
		<div class="large-33"> 
			<div class="content-icons">
				<i class="icon-folder"></i> 
			</div>
			<div class="content-text">
				<h4>Organized</h4>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi possimus, sequi, eligendi, culpa quod provident vero dolorum tempora, dignissimos consequatur laudantium perferendis!</p>
			</div>
		</div>
		<div class="large-33"> 
			<div class="content-icons">
				<i class="icon-code-1"></i> 
			</div>
			<div class="content-text">
				<h4>Simple Code</h4>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi possimus, sequi, eligendi, culpa quod provident vero dolorum tempora, dignissimos consequatur laudantium perferendis!</p>
			</div>
		</div>
	</div>
</div>
<div class="description-stewed">
	<div class="grid-11">
		<div class="large-50">
			<div class="description-view">
				<img src="assents/img/slider-pic2.png" alt="">
			</div>
		</div>	
		<div class="large-50">
			<div class="description-text">
				<span class="space-30"></span>
				<h3>Lorem Ipsum</h3>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto dolorem quis perferendis magni eum amet non consequatur eius. Dolorem natus ab, quos optio repellendus officia facilis sed aperiam, illum nulla.</p>
			</div>
		</div>	
	</div>
</div>