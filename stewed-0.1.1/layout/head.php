<title>Stewed - Responsive Framework</title>  

<link rel="icon" href="images/icon.png" type="image/x-icon" />
<link rel="shortcut icon" href="images/icon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="images/icon.png" type="image/x-icon" />
<link rel="stylesheet" href="css/stewed-0.1.1.css">        
<link rel="stylesheet" href="assents/css/syntaxhighlighter.css">
<link rel="stylesheet" href="assents/css/style.css">
<link rel="stylesheet" href="themes/default/default.css">
<script type="text/javascript " src="js/modernizr.js "></script>