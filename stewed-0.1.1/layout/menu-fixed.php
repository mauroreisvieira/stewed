<div class="nav-fixed">
	<nav class="navigation">
		<ul class="menu vertical white">
			<li><a href="#icon_menu">Graphic Icons</a>
				<ul>
					<li><a href="" class="linkdisabled">Application Icons</a></li>
					<li><a href="" class="linkdisabled">Control Icons</a></li>      
					<li><a href="" class="linkdisabled">Text Editor Icons</a></li>           
					<li><a href="" class="linkdisabled">Directional Icons</a></li>
					<li><a href="" class="linkdisabled">Temperature Icons</a></li>
					<li><a href="" class="linkdisabled">Social Icons</a></li>
				</ul>
			</li>
			<li><a href="#link_responsive">Responsive Grid</a>
			</li>
			<li><a href="#link_button">Button</a>
			</li>
			<li><a href="#link_pagination">Pagination</a>
			</li>
			<li><a href="#link-nav">Navigation (Menu)</a>
			</li>
			<li><a href="#link_alert">Alert</a></li>
			<li><a href="#link_slider">Flexslider</a></li>
			<li><a href="#link_modal">Modal</a></li>
			<li><a href="#link_toggle">Toggle Accordion</a>
			</li>
			<li><a href="#link_tabs">Nav Tabs</a>
			</li>
			<li></li>
			<li class="backTop"><a href="">Back to top <i class="icon-up-open-big"></i></a>
			</li>
		</ul>
	</nav>
</div>