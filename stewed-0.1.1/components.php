<div class="row">
	<div class="intro">
		<div class="grid-11">
			<p><span>Components UI</span>{ Responsive Framework }</p>
		</div>
	</div>
</div>
<container class="container">
	<!-- Include Menu Fixed -->

	<div class="grid-11">
		<div class="large-75">
			<!-- Include Icons -->
			<?php include 'assents/icons.php'; ?>
			<!-- Include Responsive -->
			<?php include 'assents/responsive.php'; ?>
			<!-- Include Buttons -->
			<?php include 'assents/buttons.php'; ?>
			<!-- Include Heading -->
			<?php include 'assents/heading.php'; ?>
			<!-- Include Text -->
			<?php include 'assents/text.php'; ?>
			<!-- Include Pagination -->
			<?php include 'assents/pagination.php'; ?>
			<!-- Include Menu -->
			<?php include 'assents/nav.php'; ?>
			<!-- Include Alert -->
			<?php include 'assents/alert.php'; ?>
			<!-- Include Modal -->
			<?php include 'assents/modal.php'; ?>
			<!-- Include Flexislider -->
			<?php include 'assents/flexislider.php'; ?>				
			<!-- Include Accordion -->
			<?php  include 'assents/accordion.php'; ?>				
			<!-- Include Nav Tabs -->
			<?php  include 'assents/nav-tabs.php'; ?>
		</div>
	</div>
	<?php include 'layout/menu-fixed.php'; ?>
</container>