<!-- toggle accordion -->
<section id="link_toggle" data-spy="true" data-target="#link_toggle">
	<h1 class="header-line">Toggle Accordion</h1>
	<div class="accordion-group">
		<!-- #1 -->
		<div class="toggle-default">
			<a href="#toggle-1" rel="accordion">
				<div class="header-accordion light-gray">
					<p>#1 - Toggle Accordion</p>
				</div>
			</a>
			<div class="container-accordion" id="toggle-1">
				<p class="">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.</p>
			</div>
		</div>
		<!-- #2 -->
		<div class="toggle-default">
			<a href="#toggle-2" rel="accordion">
				<div class="header-accordion light-gray">
					<p>#2 - Toggle Accordion</p>
				</div>
			</a>
			<div class="container-accordion" id="toggle-2">
				<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.</p>
			</div>
		</div>
		<!-- #3 -->
		<div class="toggle-default">
			<a href="#toggle-3" rel="accordion">
				<div class="header-accordion light-gray">
					<p>#3 - Toggle Accordion</p>
				</div>
			</a>
			<div class="container-accordion" id="toggle-3">
				<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.</p>
			</div>
		</div>
	</div>
</section>
<aside>
	<div id="view-html" class="code">
		<pre class="brush: xml; toolbar: false; gutter: false;">
			 &lt;div class="toggle-default">
				 &lt;a href="#toggle-1" rel="accordion">
					 &lt;div class="header-accordion">
						&lt;p>#3 - Toggle Accordion&lt;/p>
					&lt;/div>
				&lt;/a>
				&lt;div class="container-accordion" id="toggle-1">
					&lt;p>Anim pariatur cliche reprehenderit...&lt;/p>
				&lt;/div>
			&lt;/div>
		</pre>
	</div>
</aside>