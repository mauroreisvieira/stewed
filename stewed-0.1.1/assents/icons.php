<!-- ICONS FONTELLO -->
<section id="icon_menu" data-spy="true" data-target="#icon_menu">
	<h1 class="header-line">Application Graphic Icons</h1>
	<div id="icons" class="">
		<div class="row">
			<!-- Application Icons -->
			<h3>Application Icons</h3>
			<div title="Code: 0xea27" class="large-16 medium-25 small-33">
				<i class="icon-glass"></i>
				<span class="i-name">icon-glass</span>
				<span class="i-code">0xea27</span>
			</div>
			<div title="Code: 0xe8d2" class="large-16 medium-25 small-33">
				<i class="icon-windy-inv"></i>
				<span class="i-name">icon-windy-inv</span>
				<span class="i-code">0xe8d2</span>
			</div>
			<div title="Code: 0xea2b" class="large-16 medium-25 small-33">
				<i class="icon-mail-alt"></i>
				<span class="i-name">icon-mail-alt</span>
				<span class="i-code">0xea2b</span>
			</div>
			<div title="Code: 0xea79" class="large-16 medium-25 small-33">
				<i class="icon-male"></i>
				<span class="i-name">icon-male</span>
				<span class="i-code">0xea79</span>
			</div>
			<div title="Code: 0xea7a" class="large-16 medium-25 small-33">
				<i class="icon-female"></i>
				<span class="i-name">icon-female</span>
				<span class="i-code">0xea7a</span>
			</div>
			<div title="Code: 0xea7b" class="large-16 medium-25 small-33">
				<i class="icon-video"></i>
				<span class="i-name">icon-video</span>
				<span class="i-code">0xea7b</span>
			</div>
			<div title="Code: 0xea7c" class="large-16 medium-25 small-33">
				<i class="icon-videocam"></i>
				<span class="i-name">icon-videocam</span>
				<span class="i-code">0xea7c</span>
			</div>
			<div title="Code: 0xea89" class="large-16 medium-25 small-33">
				<i class="icon-picture"></i>
				<span class="i-name">icon-picture</span>
				<span class="i-code">0xea89</span>
			</div>

			<div title="Code: 0xe869" class="large-16 medium-25 small-33">
				<i class="icon-camera"></i>
				<span class="i-name">icon-camera</span>
				<span class="i-code">0xe869</span>
			</div>
			<div title="Code: 0xe868" class="large-16 medium-25 small-33">
				<i class="icon-camera-alt"></i>
				<span class="i-name">icon-camera-alt</span>
				<span class="i-code">0xe868</span>
			</div>
			<div title="Code: 0xe804" class="large-16 medium-25 small-33">
				<i class="icon-plus-squared-1"></i>
				<span class="i-name">icon-plus-squared-1</span>
				<span class="i-code">0xe804</span>
			</div>
			<div title="Code: 0xe86b" class="large-16 medium-25 small-33">
				<i class="icon-help-1"></i>
				<span class="i-name">icon-help-1</span>

			</div>
			<div title="Code: 0xe801" class="large-16 medium-25 small-33">
				<i class="icon-help-circled"></i>
				<span class="i-name">icon-help-circled</span>

			</div>
			<div title="Code: 0xea3e" class="large-16 medium-25 small-33">
				<i class="icon-home"></i>
				<span class="i-name">icon-home</span>

			</div>
			<div title="Code: 0xea3f" class="large-16 medium-25 small-33">
				<i class="icon-link"></i>
				<span class="i-name">icon-link</span>

			</div>
			<div title="Code: 0xea40" class="large-16 medium-25 small-33">
				<i class="icon-unlink"></i>
				<span class="i-name">icon-unlink</span>
				<span class="i-code">0xea40</span>
			</div>

			<div title="Code: 0xea41" class="large-16 medium-25 small-33">
				<i class="icon-link-ext"></i>
				<span class="i-name">icon-link-ext</span>
				<span class="i-code">0xea41</span>
			</div>
			<div title="Code: 0xea42" class="large-16 medium-25 small-33">
				<i class="icon-link-ext-alt"></i>
				<span class="i-name">icon-link-ext-alt</span>
				<span class="i-code">0xea42</span>
			</div>
			<div title="Code: 0xea43" class="large-16 medium-25 small-33">
				<i class="icon-attach-1"></i>
				<span class="i-name">icon-attach-1</span>
				<span class="i-code">0xea43</span>
			</div>
			<div title="Code: 0xea44" class="large-16 medium-25 small-33">
				<i class="icon-lock"></i>
				<span class="i-name">icon-lock</span>
				<span class="i-code">0xea44</span>
			</div>

			<div title="Code: 0xea45" class="large-16 medium-25 small-33">
				<i class="icon-lock-open"></i>
				<span class="i-name">icon-lock-open</span>
				<span class="i-code">0xea45</span>
			</div>
			<div title="Code: 0xea46" class="large-16 medium-25 small-33">
				<i class="icon-lock-open-alt"></i>
				<span class="i-name">icon-lock-open-alt</span>
				<span class="i-code">0xea46</span>
			</div>
			<div title="Code: 0xea77" class="large-16 medium-25 small-33">
				<i class="icon-pin"></i>
				<span class="i-name">icon-pin</span>
				<span class="i-code">0xea77</span>
			</div>
			<div title="Code: 0xea81" class="large-16 medium-25 small-33">
				<i class="icon-eye-1"></i>
				<span class="i-name">icon-eye-1</span>
				<span class="i-code">0xea81</span>
			</div>

			<div title="Code: 0xea82" class="large-16 medium-25 small-33">
				<i class="icon-eye-off"></i>
				<span class="i-name">icon-eye-off</span>
				<span class="i-code">0xea82</span>
			</div>
			<div title="Code: 0xea83" class="large-16 medium-25 small-33">
				<i class="icon-tag"></i>
				<span class="i-name">icon-tag</span>
				<span class="i-code">0xea83</span>
			</div>
			<div title="Code: 0xea84" class="large-16 medium-25 small-33">
				<i class="icon-tags"></i>
				<span class="i-name">icon-tags</span>
				<span class="i-code">0xea84</span>
			</div>
			<div title="Code: 0xe86d" class="large-16 medium-25 small-33">
				<i class="icon-bookmark"></i>
				<span class="i-name">icon-bookmark</span>
				<span class="i-code">0xe86d</span>
			</div>
			<div title="Code: 0xe86c" class="large-16 medium-25 small-33">
				<i class="icon-bookmark-empty"></i>
				<span class="i-name">icon-bookmark-empty</span>
				<span class="i-code">0xe86c</span>
			</div>
			<div title="Code: 0xe867" class="large-16 medium-25 small-33">
				<i class="icon-flag-1"></i>
				<span class="i-name">icon-flag-1</span>
				<span class="i-code">0xe867</span>
			</div>
			<div title="Code: 0xea47" class="large-16 medium-25 small-33">
				<i class="icon-flag-empty"></i>
				<span class="i-name">icon-flag-empty</span>
				<span class="i-code">0xea47</span>
			</div>
			<div title="Code: 0xea4d" class="large-16 medium-25 small-33">
				<i class="icon-download-1"></i>
				<span class="i-name">icon-download-1</span>
				<span class="i-code">0xea4d</span>
			</div>

			<div title="Code: 0xea4e" class="large-16 medium-25 small-33">
				<i class="icon-upload-1"></i>
				<span class="i-name">icon-upload-1</span>
				<span class="i-code">0xea4e</span>
			</div>
			<div title="Code: 0xea4f" class="large-16 medium-25 small-33">
				<i class="icon-download-cloud"></i>
				<span class="i-name">icon-download-cloud</span>
				<span class="i-code">0xea4f</span>
			</div>
			<div title="Code: 0xea50" class="large-16 medium-25 small-33">
				<i class="icon-upload-cloud"></i>
				<span class="i-name">icon-upload-cloud</span>
				<span class="i-code">0xea50</span>
			</div>

			<div title="Code: 0xea85" class="large-16 medium-25 small-33">
				<i class="icon-quote-left"></i>
				<span class="i-name">icon-quote-left</span>
				<span class="i-code">0xea85</span>
			</div>
			<div title="Code: 0xea86" class="large-16 medium-25 small-33">
				<i class="icon-quote-right"></i>
				<span class="i-name">icon-quote-right</span>
				<span class="i-code">0xea86</span>
			</div>

			<div title="Code: 0xea87" class="large-16 medium-25 small-33">
				<i class="icon-code-1"></i>
				<span class="i-name">icon-code-1</span>
				<span class="i-code">0xea87</span>
			</div>
			<div title="Code: 0xea56" class="large-16 medium-25 small-33">
				<i class="icon-keyboard-1"></i>
				<span class="i-name">icon-keyboard-1</span>
				<span class="i-code">0xea56</span>
			</div>
			<div title="Code: 0xea57" class="large-16 medium-25 small-33">
				<i class="icon-gamepad"></i>
				<span class="i-name">icon-gamepad</span>
				<span class="i-code">0xea57</span>
			</div>
			<div title="Code: 0xea58" class="large-16 medium-25 small-33">
				<i class="icon-comment-1"></i>
				<span class="i-name">icon-comment-1</span>
				<span class="i-code">0xea58</span>
			</div>
			<div title="Code: 0xea59" class="large-16 medium-25 small-33">
				<i class="icon-chat-1"></i>
				<span class="i-name">icon-chat-1</span>
				<span class="i-code">0xea59</span>
			</div>
			<div title="Code: 0xea5a" class="large-16 medium-25 small-33">
				<i class="icon-comment-empty"></i>
				<span class="i-name">icon-comment-empty</span>
				<span class="i-code">0xea5a</span>
			</div>
			<div title="Code: 0xea5b" class="large-16 medium-25 small-33">
				<i class="icon-chat-empty"></i>
				<span class="i-name">icon-chat-empty</span>
				<span class="i-code">0xea5b</span>
			</div>
			<div title="Code: 0xea5c" class="large-16 medium-25 small-33">
				<i class="icon-bell-1"></i>
				<span class="i-name">icon-bell-1</span>
				<span class="i-code">0xea5c</span>
			</div>
			<div title="Code: 0xea5d" class="large-16 medium-25 small-33">
				<i class="icon-bell-alt"></i>
				<span class="i-name">icon-bell-alt</span>
				<span class="i-code">0xea5d</span>
			</div>
			<div title="Code: 0xea5e" class="large-16 medium-25 small-33">
				<i class="icon-attention-alt"></i>
				<span class="i-name">icon-attention-alt</span>
				<span class="i-code">0xea5e</span>
			</div>
			<div title="Code: 0xe802" class="large-16 medium-25 small-33">
				<i class="icon-attention-1"></i>
				<span class="i-name">icon-attention-1</span>
				<span class="i-code">0xe802</span>
			</div>
			<div title="Code: 0xea25" class="large-16 medium-25 small-33">
				<i class="icon-attention-circled"></i>
				<span class="i-name">icon-attention-circled</span>
				<span class="i-code">0xea25</span>
			</div>
			<div title="Code: 0xea26" class="large-16 medium-25 small-33">
				<i class="icon-location-1"></i>
				<span class="i-name">icon-location-1</span>
				<span class="i-code">0xea26</span>
			</div>
			<div title="Code: 0xe876" class="large-16 medium-25 small-33">
				<i class="icon-direction"></i>
				<span class="i-name">icon-direction</span>
				<span class="i-code">0xe876</span>
			</div>
			<div title="Code: 0xe875" class="large-16 medium-25 small-33">
				<i class="icon-compass-2"></i>
				<span class="i-name">icon-compass-2</span>
				<span class="i-code">0xe875</span>
			</div>

			<div title="Code: 0xea67" class="large-16 medium-25 small-33">
				<i class="icon-rss-1"></i>
				<span class="i-name">icon-rss-1</span>
				<span class="i-code">0xea67</span>
			</div>
			<div title="Code: 0xea68" class="large-16 medium-25 small-33">
				<i class="icon-rss-squared"></i>
				<span class="i-name">icon-rss-squared</span>
				<span class="i-code">0xea68</span>
			</div>
			<div title="Code: 0xea69" class="large-16 medium-25 small-33">
				<i class="icon-phone-1"></i>
				<span class="i-name">icon-phone-1</span>
				<span class="i-code">0xea69</span>
			</div>
			<div title="Code: 0xea6a" class="large-16 medium-25 small-33">
				<i class="icon-phone-squared"></i>
				<span class="i-name">icon-phone-squared</span>
				<span class="i-code">0xea6a</span>
			</div>
			<div title="Code: 0xe803" class="large-16 medium-25 small-33">
				<i class="icon-menu"></i>
				<span class="i-name">icon-menu</span>
				<span class="i-code">0xe803</span>
			</div>
			<div title="Code: 0xea24" class="large-16 medium-25 small-33">
				<i class="icon-cog-1"></i>
				<span class="i-name">icon-cog-1</span>
				<span class="i-code">0xea24</span>
			</div>
			<div title="Code: 0xe806" class="large-16 medium-25 small-33">
				<i class="icon-cog-alt"></i>
				<span class="i-name">icon-cog-alt</span>
				<span class="i-code">0xe806</span>
			</div>
			<div title="Code: 0xe877" class="large-16 medium-25 small-33">
				<i class="icon-wrench"></i>
				<span class="i-name">icon-wrench</span>
				<span class="i-code">0xe877</span>
			</div>
			<div title="Code: 0xe874" class="large-16 medium-25 small-33">
				<i class="icon-basket"></i>
				<span class="i-name">icon-basket</span>
				<span class="i-code">0xe874</span>
			</div>
			<div title="Code: 0xe872" class="large-16 medium-25 small-33">
				<i class="icon-calendar"></i>
				<span class="i-name">icon-calendar</span>
				<span class="i-code">0xe872</span>
			</div>
			<div title="Code: 0xe873" class="large-16 medium-25 small-33">
				<i class="icon-calendar-empty"></i>
				<span class="i-name">icon-calendar-empty</span>
				<span class="i-code">0xe873</span>
			</div>
			<div title="Code: 0xe864" class="large-16 medium-25 small-33">
				<i class="icon-login"></i>
				<span class="i-name">icon-login</span>
				<span class="i-code">0xe864</span>
			</div>
			<div title="Code: 0xea6b" class="large-16 medium-25 small-33">
				<i class="icon-logout-1"></i>
				<span class="i-name">icon-logout-1</span>
				<span class="i-code">0xea6b</span>
			</div>
			<div title="Code: 0xea6c" class="large-16 medium-25 small-33">
				<i class="icon-mic-1"></i>
				<span class="i-name">icon-mic-1</span>
				<span class="i-code">0xea6c</span>
			</div>
			<div title="Code: 0xea6d" class="large-16 medium-25 small-33">
				<i class="icon-mute-1"></i>
				<span class="i-name">icon-mute-1</span>
				<span class="i-code">0xea6d</span>
			</div>
			<div title="Code: 0xea6e" class="large-16 medium-25 small-33">
				<i class="icon-volume-off"></i>
				<span class="i-name">icon-volume-off</span>
				<span class="i-code">0xea6e</span>
			</div>
			<div title="Code: 0xea6f" class="large-16 medium-25 small-33">
				<i class="icon-volume-down"></i>
				<span class="i-name">icon-volume-down</span>
				<span class="i-code">0xea6f</span>
			</div>
			<div title="Code: 0xea70" class="large-16 medium-25 small-33">
				<i class="icon-volume-up"></i>
				<span class="i-name">icon-volume-up</span>
				<span class="i-code">0xea70</span>
			</div>
			<div title="Code: 0xea71" class="large-16 medium-25 small-33">
				<i class="icon-headphones"></i>
				<span class="i-name">icon-headphones</span>
				<span class="i-code">0xea71</span>
			</div>

			<div title="Code: 0xea72" class="large-16 medium-25 small-33">
				<i class="icon-clock-1"></i>
				<span class="i-name">icon-clock-1</span>
				<span class="i-code">0xea72</span>
			</div>
			<div title="Code: 0xea73" class="large-16 medium-25 small-33">
				<i class="icon-lightbulb"></i>
				<span class="i-name">icon-lightbulb</span>
				<span class="i-code">0xea73</span>
			</div>
			<div title="Code: 0xea74" class="large-16 medium-25 small-33">
				<i class="icon-block-1"></i>
				<span class="i-name">icon-block-1</span>
				<span class="i-code">0xea74</span>
			</div>

			<div title="Code: 0xe9e5" class="large-16 medium-25 small-33">
				<i class="icon-target-1"></i>
				<span class="i-name">icon-target-1</span>
				<span class="i-code">0xe9e5</span>
			</div>
			<div title="Code: 0xe9e6" class="large-16 medium-25 small-33">
				<i class="icon-signal-1"></i>
				<span class="i-name">icon-signal-1</span>
				<span class="i-code">0xe9e6</span>
			</div>
			<div title="Code: 0xea06" class="large-16 medium-25 small-33">
				<i class="icon-award"></i>
				<span class="i-name">icon-award</span>
				<span class="i-code">0xea06</span>
			</div>
			<div title="Code: 0xea07" class="large-16 medium-25 small-33">
				<i class="icon-desktop"></i>
				<span class="i-name">icon-desktop</span>
				<span class="i-code">0xea07</span>
			</div>
			<div title="Code: 0xea08" class="large-16 medium-25 small-33">
				<i class="icon-laptop"></i>
				<span class="i-name">icon-laptop</span>
			</div>
			<div title="Code: 0xea09" class="large-16 medium-25 small-33">
				<i class="icon-tablet"></i>
				<span class="i-name">icon-tablet</span>
			</div>
			<div title="Code: 0xea0a" class="large-16 medium-25 small-33">
				<i class="icon-mobile"></i>
				<span class="i-name">icon-mobile</span>
			</div>
			<div title="Code: 0xea0b" class="large-16 medium-25 small-33">
				<i class="icon-inbox-1"></i>
				<span class="i-name">icon-inbox-1</span>
				<span class="i-code">0xea0b</span>
			</div>
			<div title="Code: 0xea0c" class="large-16 medium-25 small-33">
				<i class="icon-globe-1"></i>
				<span class="i-name">icon-globe-1</span>
				<span class="i-code">0xea0c</span>
			</div>
			<div title="Code: 0xea0d" class="large-16 medium-25 small-33">
				<i class="icon-sun"></i>
				<span class="i-name">icon-sun</span>
				<span class="i-code">0xea0d</span>
			</div>
			<div title="Code: 0xea0f" class="large-16 medium-25 small-33">
				<i class="icon-flash-1"></i>
				<span class="i-name">icon-flash-1</span>
				<span class="i-code">0xea0f</span>
			</div>
			<div title="Code: 0xea11" class="large-16 medium-25 small-33">
				<i class="icon-umbrella"></i>
				<span class="i-name">icon-umbrella</span>
				<span class="i-code">0xea11</span>
			</div>
			<div title="Code: 0xe81d" class="large-16 medium-25 small-33">
				<i class="icon-flight-1"></i>
				<span class="i-name">icon-flight-1</span>
				<span class="i-code">0xe81d</span>
			</div>
			<div title="Code: 0xe81e" class="large-16 medium-25 small-33">
				<i class="icon-fighter-jet"></i>
				<span class="i-name">icon-fighter-jet</span>
				<span class="i-code">0xe81e</span>
			</div>
			<div title="Code: 0xe81f" class="large-16 medium-25 small-33">
				<i class="icon-leaf-1"></i>
				<span class="i-name">icon-leaf-1</span>
				<span class="i-code">0xe81f</span>
			</div>

			<div title="Code: 0xe963" class="large-16 medium-25 small-33">
				<i class="icon-briefcase-1"></i>
				<span class="i-name">icon-briefcase-1</span>
				<span class="i-code">0xe963</span>
			</div>
			<div title="Code: 0xe964" class="large-16 medium-25 small-33">
				<i class="icon-suitcase-1"></i>
				<span class="i-name">icon-suitcase-1</span>
				<span class="i-code">0xe964</span>
			</div>
			<div title="Code: 0xe965" class="large-16 medium-25 small-33">
				<i class="icon-ellipsis"></i>
				<span class="i-name">icon-ellipsis</span>
				<span class="i-code">0xe965</span>
			</div>
			<div title="Code: 0xe966" class="large-16 medium-25 small-33">
				<i class="icon-ellipsis-vert"></i>
				<span class="i-name">icon-ellipsis-vert</span>
				<span class="i-code">0xe966</span>
			</div>
			<div title="Code: 0xe967" class="large-16 medium-25 small-33">
				<i class="icon-off"></i>
				<span class="i-name">icon-off</span>
				<span class="i-code">0xe967</span>
			</div>
			<div title="Code: 0xe968" class="large-16 medium-25 small-33">
				<i class="icon-road"></i>
				<span class="i-name">icon-road</span>
				<span class="i-code">0xe968</span>
			</div>
			<div title="Code: 0xe969" class="large-16 medium-25 small-33">
				<i class="icon-list-alt"></i>
				<span class="i-name">icon-list-alt</span>
				<span class="i-code">0xe969</span>
			</div>
			<div title="Code: 0xe96a" class="large-16 medium-25 small-33">
				<i class="icon-qrcode"></i>
				<span class="i-name">icon-qrcode</span>
				<span class="i-code">0xe96a</span>
			</div>
			<div title="Code: 0xe96b" class="large-16 medium-25 small-33">
				<i class="icon-barcode"></i>
				<span class="i-name">icon-barcode</span>
				<span class="i-code">0xe96b</span>
			</div>
			<div title="Code: 0xe96c" class="large-16 medium-25 small-33">
				<i class="icon-book-1"></i>
				<span class="i-name">icon-book-1</span>
				<span class="i-code">0xe96c</span>
			</div>
			<div title="Code: 0xe96d" class="large-16 medium-25 small-33">
				<i class="icon-ajust"></i>
				<span class="i-name">icon-ajust</span>
				<span class="i-code">0xe96d</span>
			</div>
			<div title="Code: 0xe96e" class="large-16 medium-25 small-33">
				<i class="icon-tint"></i>
				<span class="i-name">icon-tint</span>
				<span class="i-code">0xe96e</span>
			</div>

			<div title="Code: 0xe971" class="large-16 medium-25 small-33">
				<i class="icon-circle"></i>
				<span class="i-name">icon-circle</span>
				<span class="i-code">0xe971</span>
			</div>
			<div title="Code: 0xe972" class="large-16 medium-25 small-33">
				<i class="icon-circle-empty"></i>
				<span class="i-name">icon-circle-empty</span>
				<span class="i-code">0xe972</span>
			</div>
			<div title="Code: 0xe973" class="large-16 medium-25 small-33">
				<i class="icon-dot-circled"></i>
				<span class="i-name">icon-dot-circled</span>
				<span class="i-code">0xe973</span>
			</div>
			<div title="Code: 0xe974" class="large-16 medium-25 small-33">
				<i class="icon-asterisk"></i>
				<span class="i-name">icon-asterisk</span>
				<span class="i-code">0xe974</span>
			</div>
			<div title="Code: 0xe975" class="large-16 medium-25 small-33">
				<i class="icon-gift"></i>
				<span class="i-name">icon-gift</span>
				<span class="i-code">0xe975</span>
			</div>
			<div title="Code: 0xe976" class="large-16 medium-25 small-33">
				<i class="icon-fire"></i>
				<span class="i-name">icon-fire</span>
				<span class="i-code">0xe976</span>
			</div>
			<div title="Code: 0xe977" class="large-16 medium-25 small-33">
				<i class="icon-magnet"></i>
				<span class="i-name">icon-magnet</span>
				<span class="i-code">0xe977</span>
			</div>
			<div title="Code: 0xe978" class="large-16 medium-25 small-33">
				<i class="icon-chart-bar-1"></i>
				<span class="i-name">icon-chart-bar-1</span>
				<span class="i-code">0xe978</span>
			</div>
			<div title="Code: 0xe979" class="large-16 medium-25 small-33">
				<i class="icon-ticket-1"></i>
				<span class="i-name">icon-ticket-1</span>
				<span class="i-code">0xe979</span>
			</div>
			<div title="Code: 0xe97a" class="large-16 medium-25 small-33">
				<i class="icon-credit-card-1"></i>
				<span class="i-name">icon-credit-card-1</span>
				<span class="i-code">0xe97a</span>
			</div>
			<div title="Code: 0xe97b" class="large-16 medium-25 small-33">
				<i class="icon-floppy-1"></i>
				<span class="i-name">icon-floppy-1</span>
				<span class="i-code">0xe97b</span>
			</div>
			<div title="Code: 0xe97c" class="large-16 medium-25 small-33">
				<i class="icon-megaphone-1"></i>
				<span class="i-name">icon-megaphone-1</span>
				<span class="i-code">0xe97c</span>
			</div>
			<div title="Code: 0xe97d" class="large-16 medium-25 small-33">
				<i class="icon-hdd"></i>
				<span class="i-name">icon-hdd</span>
				<span class="i-code">0xe97d</span>
			</div>
			<div title="Code: 0xe97e" class="large-16 medium-25 small-33">
				<i class="icon-key-1"></i>
				<span class="i-name">icon-key-1</span>
				<span class="i-code">0xe97e</span>
			</div>

			<div title="Code: 0xe980" class="large-16 medium-25 small-33">
				<i class="icon-rocket-1"></i>
				<span class="i-name">icon-rocket-1</span>
				<span class="i-code">0xe980</span>
			</div>
			<div title="Code: 0xe981" class="large-16 medium-25 small-33">
				<i class="icon-bug"></i>
				<span class="i-name">icon-bug</span>
				<span class="i-code">0xe981</span>
			</div>
			<div title="Code: 0xe982" class="large-16 medium-25 small-33">
				<i class="icon-certificate"></i>
				<span class="i-name">icon-certificate</span>
				<span class="i-code">0xe982</span>
			</div>
			<div title="Code: 0xe983" class="large-16 medium-25 small-33">
				<i class="icon-tasks"></i>
				<span class="i-name">icon-tasks</span>
				<span class="i-code">0xe983</span>
			</div>
			<div title="Code: 0xe984" class="large-16 medium-25 small-33">
				<i class="icon-filter"></i>
				<span class="i-name">icon-filter</span>
				<span class="i-code">0xe984</span>
			</div>
			<div title="Code: 0xe985" class="large-16 medium-25 small-33">
				<i class="icon-beaker"></i>
				<span class="i-name">icon-beaker</span>
				<span class="i-code">0xe985</span>
			</div>
			<div title="Code: 0xe986" class="large-16 medium-25 small-33">
				<i class="icon-magic"></i>
				<span class="i-name">icon-magic</span>
				<span class="i-code">0xe986</span>
			</div>
			<div title="Code: 0xe987" class="large-16 medium-25 small-33">
				<i class="icon-truck"></i>
				<span class="i-name">icon-truck</span>
				<span class="i-code">0xe987</span>
			</div>
			<div title="Code: 0xe988" class="large-16 medium-25 small-33">
				<i class="icon-money"></i>
				<span class="i-name">icon-money</span>
				<span class="i-code">0xe988</span>
			</div>
			<div title="Code: 0xe98b" class="large-16 medium-25 small-33">
				<i class="icon-dollar"></i>
				<span class="i-name">icon-dollar</span>
				<span class="i-code">0xe98b</span>
			</div>

			<div title="Code: 0xe99b" class="large-16 medium-25 small-33">
				<i class="icon-hammer"></i>
				<span class="i-name">icon-hammer</span>
				<span class="i-code">0xe99b</span>
			</div>
			<div title="Code: 0xe99c" class="large-16 medium-25 small-33">
				<i class="icon-gauge-1"></i>
				<span class="i-name">icon-gauge-1</span>
				<span class="i-code">0xe99c</span>
			</div>
			<div title="Code: 0xe99d" class="large-16 medium-25 small-33">
				<i class="icon-sitemap"></i>
				<span class="i-name">icon-sitemap</span>
				<span class="i-code">0xe99d</span>
			</div>
			<div title="Code: 0xe99e" class="large-16 medium-25 small-33">
				<i class="icon-spinner"></i>
				<span class="i-name">icon-spinner</span>
				<span class="i-code">0xe99e</span>
			</div>
			<div title="Code: 0xe99f" class="large-16 medium-25 small-33">
				<i class="icon-coffee"></i>
				<span class="i-name">icon-coffee</span>
				<span class="i-code">0xe99f</span>
			</div>
			<div title="Code: 0xe9a0" class="large-16 medium-25 small-33">
				<i class="icon-food"></i>
				<span class="i-name">icon-food</span>
				<span class="i-code">0xe9a0</span>
			</div>
			<div title="Code: 0xe9a1" class="large-16 medium-25 small-33">
				<i class="icon-beer"></i>
				<span class="i-name">icon-beer</span>
				<span class="i-code">0xe9a1</span>
			</div>
			<div title="Code: 0xe9a2" class="large-16 medium-25 small-33">
				<i class="icon-user-md"></i>
				<span class="i-name">icon-user-md</span>
				<span class="i-code">0xe9a2</span>
			</div>
			<div title="Code: 0xe9a3" class="large-16 medium-25 small-33">
				<i class="icon-stethoscope"></i>
				<span class="i-name">icon-stethoscope</span>
				<span class="i-code">0xe9a3</span>
			</div>
			<div title="Code: 0xe9a4" class="large-16 medium-25 small-33">
				<i class="icon-ambulance"></i>
				<span class="i-name">icon-ambulance</span>
				<span class="i-code">0xe9a4</span>
			</div>
			<div title="Code: 0xe9a5" class="large-16 medium-25 small-33">
				<i class="icon-medkit"></i>
				<span class="i-name">icon-medkit</span>
				<span class="i-code">0xe9a5</span>
			</div>
			<div title="Code: 0xe9a6" class="large-16 medium-25 small-33">
				<i class="icon-h-sigh"></i>
				<span class="i-name">icon-h-sigh</span>
				<span class="i-code">0xe9a6</span>
			</div>
			<div title="Code: 0xe9a7" class="large-16 medium-25 small-33">
				<i class="icon-hospital"></i>
				<span class="i-name">icon-hospital</span>
				<span class="i-code">0xe9a7</span>
			</div>
			<div title="Code: 0xe9a8" class="large-16 medium-25 small-33">
				<i class="icon-building"></i>
				<span class="i-name">icon-building</span>
				<span class="i-code">0xe9a8</span>
			</div>
			<div title="Code: 0xe9a9" class="large-16 medium-25 small-33">
				<i class="icon-smile"></i>
				<span class="i-name">icon-smile</span>
				<span class="i-code">0xe9a9</span>
			</div>
			<div title="Code: 0xe9aa" class="large-16 medium-25 small-33">
				<i class="icon-frown"></i>
				<span class="i-name">icon-frown</span>
				<span class="i-code">0xe9aa</span>
			</div>
			<div title="Code: 0xe9ab" class="large-16 medium-25 small-33">
				<i class="icon-meh"></i>
				<span class="i-name">icon-meh</span>
				<span class="i-code">0xe9ab</span>
			</div>
			<div title="Code: 0xe9ac" class="large-16 medium-25 small-33">
				<i class="icon-anchor"></i>
				<span class="i-name">icon-anchor</span>
				<span class="i-code">0xe9ac</span>
			</div>
			<div title="Code: 0xe9ad" class="large-16 medium-25 small-33">
				<i class="icon-terminal"></i>
				<span class="i-name">icon-terminal</span>
				<span class="i-code">0xe9ad</span>
			</div>
			<div title="Code: 0xe9ae" class="large-16 medium-25 small-33">
				<i class="icon-eraser"></i>
				<span class="i-name">icon-eraser</span>
				<span class="i-code">0xe9ae</span>
			</div>
			<div title="Code: 0xe9af" class="large-16 medium-25 small-33">
				<i class="icon-puzzle"></i>
				<span class="i-name">icon-puzzle</span>
				<span class="i-code">0xe9af</span>
			</div>
			<div title="Code: 0xe9b0" class="large-16 medium-25 small-33">
				<i class="icon-shield"></i>
				<span class="i-name">icon-shield</span>
				<span class="i-code">0xe9b0</span>
			</div>
			<div title="Code: 0xe9b1" class="large-16 medium-25 small-33">
				<i class="icon-extinguisher"></i>
				<span class="i-name">icon-extinguisher</span>
				<span class="i-code">0xe9b1</span>
			</div>
			<div title="Code: 0xe828" class="large-16 medium-25 small-33">
				<i class="icon-note"></i>
				<span class="i-name">icon-note</span>
				<span class="i-code">0xe828</span>
			</div>
			<div title="Code: 0xe829" class="large-16 medium-25 small-33">
				<i class="icon-note-beamed"></i>
				<span class="i-name">icon-note-beamed</span>
				<span class="i-code">0xe829</span>
			</div>
			<div title="Code: 0xe82a" class="large-16 medium-25 small-33">
				<i class="icon-music"></i>
				<span class="i-name">icon-music</span>
				<span class="i-code">0xe82a</span>
			</div>
			<div title="Code: 0xe82b" class="large-16 medium-25 small-33">
				<i class="icon-search"></i>
				<span class="i-name">icon-search</span>
				<span class="i-code">0xe82b</span>
			</div>
			<div title="Code: 0xe80c" class="large-16 medium-25 small-33">
				<i class="icon-heart"></i>
				<span class="i-name">icon-heart</span>
				<span class="i-code">0xe80c</span>
			</div>
			<div title="Code: 0xe80b" class="large-16 medium-25 small-33">
				<i class="icon-heart-empty"></i>
				<span class="i-name">icon-heart-empty</span>
				<span class="i-code">0xe80b</span>
			</div>
			<div title="Code: 0xe809" class="large-16 medium-25 small-33">
				<i class="icon-star"></i>
				<span class="i-name">icon-star</span>
				<span class="i-code">0xe809</span>
			</div>
			<div title="Code: 0xe80a" class="large-16 medium-25 small-33">
				<i class="icon-star-empty"></i>
				<span class="i-name">icon-star-empty</span>
				<span class="i-code">0xe80a</span>
			</div>
			<div title="Code: 0xe832" class="large-16 medium-25 small-33">
				<i class="icon-user-1"></i>
				<span class="i-name">icon-user-1</span>
				<span class="i-code">0xe832</span>
			</div>
			<div title="Code: 0xe833" class="large-16 medium-25 small-33">
				<i class="icon-users-1"></i>
				<span class="i-name">icon-users-1</span>
				<span class="i-code">0xe833</span>
			</div>
			<div title="Code: 0xe834" class="large-16 medium-25 small-33">
				<i class="icon-user-add"></i>
				<span class="i-name">icon-user-add</span>
				<span class="i-code">0xe834</span>
			</div>
			<div title="Code: 0xe835" class="large-16 medium-25 small-33">
				<i class="icon-video-1"></i>
				<span class="i-name">icon-video-1</span>
				<span class="i-code">0xe835</span>
			</div>
			<div title="Code: 0xe836" class="large-16 medium-25 small-33">
				<i class="icon-picture-1"></i>
				<span class="i-name">icon-picture-1</span>
				<span class="i-code">0xe836</span>
			</div>
			<div title="Code: 0xe837" class="large-16 medium-25 small-33">
				<i class="icon-camera-1"></i>
				<span class="i-name">icon-camera-1</span>
				<span class="i-code">0xe837</span>
			</div>
			<div title="Code: 0xe838" class="large-16 medium-25 small-33">
				<i class="icon-layout"></i>
				<span class="i-name">icon-layout</span>
				<span class="i-code">0xe838</span>
			</div>


			<div title="Code: 0xe844" class="large-16 medium-25 small-33">
				<i class="icon-help"></i>
				<span class="i-name">icon-help</span>
				<span class="i-code">0xe844</span>
			</div>
			<div title="Code: 0xe845" class="large-16 medium-25 small-33">
				<i class="icon-help-circled-1"></i>
				<span class="i-name">icon-help-circled-1</span>
				<span class="i-code">0xe845</span>
			</div>
			<div title="Code: 0xe846" class="large-16 medium-25 small-33">
				<i class="icon-info"></i>
				<span class="i-name">icon-info</span>
				<span class="i-code">0xe846</span>
			</div>
			<div title="Code: 0xe847" class="large-16 medium-25 small-33">
				<i class="icon-info-circled"></i>
				<span class="i-name">icon-info-circled</span>
				<span class="i-code">0xe847</span>
			</div>

			<div title="Code: 0xe849" class="large-16 medium-25 small-33">
				<i class="icon-home-1"></i>
				<span class="i-name">icon-home-1</span>
				<span class="i-code">0xe849</span>
			</div>
			<div title="Code: 0xe84a" class="large-16 medium-25 small-33">
				<i class="icon-link-1"></i>
				<span class="i-name">icon-link-1</span>
				<span class="i-code">0xe84a</span>
			</div>
			<div title="Code: 0xe84b" class="large-16 medium-25 small-33">
				<i class="icon-attach"></i>
				<span class="i-name">icon-attach</span>
				<span class="i-code">0xe84b</span>
			</div>
			<div title="Code: 0xe84f" class="large-16 medium-25 small-33">
				<i class="icon-tag-1"></i>
				<span class="i-name">icon-tag-1</span>
				<span class="i-code">0xe84f</span>
			</div>
			<div title="Code: 0xe850" class="large-16 medium-25 small-33">
				<i class="icon-bookmark-1"></i>
				<span class="i-name">icon-bookmark-1</span>
				<span class="i-code">0xe850</span>
			</div>
			<div title="Code: 0xe851" class="large-16 medium-25 small-33">
				<i class="icon-bookmarks"></i>
				<span class="i-name">icon-bookmarks</span>
				<span class="i-code">0xe851</span>
			</div>
			<div title="Code: 0xe852" class="large-16 medium-25 small-33">
				<i class="icon-flag"></i>
				<span class="i-name">icon-flag</span>
				<span class="i-code">0xe852</span>
			</div>
			<div title="Code: 0xe853" class="large-16 medium-25 small-33">
				<i class="icon-thumbs-up"></i>
				<span class="i-name">icon-thumbs-up</span>
				<span class="i-code">0xe853</span>
			</div>
			<div title="Code: 0xe859" class="large-16 medium-25 small-33">
				<i class="icon-thumbs-down"></i>
				<span class="i-name">icon-thumbs-down</span>
				<span class="i-code">0xe859</span>
			</div>
			<div title="Code: 0xe85a" class="large-16 medium-25 small-33">
				<i class="icon-download"></i>
				<span class="i-name">icon-download</span>
				<span class="i-code">0xe85a</span>
			</div>
			<div title="Code: 0xe85b" class="large-16 medium-25 small-33">
				<i class="icon-upload"></i>
				<span class="i-name">icon-upload</span>
				<span class="i-code">0xe85b</span>
			</div>
			<div title="Code: 0xe85c" class="large-16 medium-25 small-33">
				<i class="icon-upload-cloud-1"></i>
				<span class="i-name">icon-upload-cloud-1</span>
				<span class="i-code">0xe85c</span>
			</div>

			<div title="Code: 0xe880" class="large-16 medium-25 small-33">
				<i class="icon-keyboard"></i>
				<span class="i-name">icon-keyboard</span>
				<span class="i-code">0xe880</span>
			</div>
			<div title="Code: 0xe881" class="large-16 medium-25 small-33">
				<i class="icon-comment"></i>
				<span class="i-name">icon-comment</span>
				<span class="i-code">0xe881</span>
			</div>
			<div title="Code: 0xe882" class="large-16 medium-25 small-33">
				<i class="icon-chat"></i>
				<span class="i-name">icon-chat</span>
				<span class="i-code">0xe882</span>
			</div>
			<div title="Code: 0xe883" class="large-16 medium-25 small-33">
				<i class="icon-bell"></i>
				<span class="i-name">icon-bell</span>
				<span class="i-code">0xe883</span>
			</div>
			<div title="Code: 0xe884" class="large-16 medium-25 small-33">
				<i class="icon-attention"></i>
				<span class="i-name">icon-attention</span>
				<span class="i-code">0xe884</span>
			</div>
			<div title="Code: 0xe885" class="large-16 medium-25 small-33">
				<i class="icon-alert"></i>
				<span class="i-name">icon-alert</span>
				<span class="i-code">0xe885</span>
			</div>
			<div title="Code: 0xe886" class="large-16 medium-25 small-33">
				<i class="icon-vcard"></i>
				<span class="i-name">icon-vcard</span>
				<span class="i-code">0xe886</span>
			</div>
			<div title="Code: 0xe887" class="large-16 medium-25 small-33">
				<i class="icon-address"></i>
				<span class="i-name">icon-address</span>
				<span class="i-code">0xe887</span>
			</div>
			<div title="Code: 0xe888" class="large-16 medium-25 small-33">
				<i class="icon-location"></i>
				<span class="i-name">icon-location</span>
				<span class="i-code">0xe888</span>
			</div>
			<div title="Code: 0xe889" class="large-16 medium-25 small-33">
				<i class="icon-map"></i>
				<span class="i-name">icon-map</span>
				<span class="i-code">0xe889</span>
			</div>
			<div title="Code: 0xe88a" class="large-16 medium-25 small-33">
				<i class="icon-direction-1"></i>
				<span class="i-name">icon-direction-1</span>
				<span class="i-code">0xe88a</span>
			</div>
			<div title="Code: 0xe88b" class="large-16 medium-25 small-33">
				<i class="icon-compass-1"></i>
				<span class="i-name">icon-compass-1</span>
				<span class="i-code">0xe88b</span>
			</div>
			<div title="Code: 0xe88c" class="large-16 medium-25 small-33">
				<i class="icon-cup"></i>
				<span class="i-name">icon-cup</span>
				<span class="i-code">0xe88c</span>
			</div>

			<div title="Code: 0xe899" class="large-16 medium-25 small-33">
				<i class="icon-rss"></i>
				<span class="i-name">icon-rss</span>
				<span class="i-code">0xe899</span>
			</div>
			<div title="Code: 0xe89a" class="large-16 medium-25 small-33">
				<i class="icon-phone"></i>
				<span class="i-name">icon-phone</span>
				<span class="i-code">0xe89a</span>
			</div>
			<div title="Code: 0xe89b" class="large-16 medium-25 small-33">
				<i class="icon-cog"></i>
				<span class="i-name">icon-cog</span>
				<span class="i-code">0xe89b</span>
			</div>
			<div title="Code: 0xe89c" class="large-16 medium-25 small-33">
				<i class="icon-tools"></i>
				<span class="i-name">icon-tools</span>
				<span class="i-code">0xe89c</span>
			</div>
			<div title="Code: 0xe89d" class="large-16 medium-25 small-33">
				<i class="icon-share"></i>
				<span class="i-name">icon-share</span>
				<span class="i-code">0xe89d</span>
			</div>
			<div title="Code: 0xe89e" class="large-16 medium-25 small-33">
				<i class="icon-shareable"></i>
				<span class="i-name">icon-shareable</span>
				<span class="i-code">0xe89e</span>
			</div>
			<div title="Code: 0xe89f" class="large-16 medium-25 small-33">
				<i class="icon-basket-1"></i>
				<span class="i-name">icon-basket-1</span>
				<span class="i-code">0xe89f</span>
			</div>
			<div title="Code: 0xe8a0" class="large-16 medium-25 small-33">
				<i class="icon-bag"></i>
				<span class="i-name">icon-bag</span>
				<span class="i-code">0xe8a0</span>
			</div>
			<div title="Code: 0xe8a1" class="large-16 medium-25 small-33">
				<i class="icon-calendar-1"></i>
				<span class="i-name">icon-calendar-1</span>
				<span class="i-code">0xe8a1</span>
			</div>
			<div title="Code: 0xe8a2" class="large-16 medium-25 small-33">
				<i class="icon-login-1"></i>
				<span class="i-name">icon-login-1</span>
				<span class="i-code">0xe8a2</span>
			</div>
			<div title="Code: 0xe8a3" class="large-16 medium-25 small-33">
				<i class="icon-logout"></i>
				<span class="i-name">icon-logout</span>
				<span class="i-code">0xe8a3</span>
			</div>
			<div title="Code: 0xe8a4" class="large-16 medium-25 small-33">
				<i class="icon-mic"></i>
				<span class="i-name">icon-mic</span>
				<span class="i-code">0xe8a4</span>
			</div>
			<div title="Code: 0xe8a5" class="large-16 medium-25 small-33">
				<i class="icon-mute"></i>
				<span class="i-name">icon-mute</span>
				<span class="i-code">0xe8a5</span>
			</div>
			<div title="Code: 0xe8a6" class="large-16 medium-25 small-33">
				<i class="icon-sound"></i>
				<span class="i-name">icon-sound</span>
				<span class="i-code">0xe8a6</span>
			</div>
			<div title="Code: 0xe8a7" class="large-16 medium-25 small-33">
				<i class="icon-volume"></i>
				<span class="i-name">icon-volume</span>
				<span class="i-code">0xe8a7</span>
			</div>
			<div title="Code: 0xe8a8" class="large-16 medium-25 small-33">
				<i class="icon-clock"></i>
				<span class="i-name">icon-clock</span>
				<span class="i-code">0xe8a8</span>
			</div>
			<div title="Code: 0xe8a9" class="large-16 medium-25 small-33">
				<i class="icon-hourglass"></i>
				<span class="i-name">icon-hourglass</span>
				<span class="i-code">0xe8a9</span>
			</div>
			<div title="Code: 0xe8aa" class="large-16 medium-25 small-33">
				<i class="icon-lamp"></i>
				<span class="i-name">icon-lamp</span>
				<span class="i-code">0xe8aa</span>
			</div>
			<div title="Code: 0xe8ab" class="large-16 medium-25 small-33">
				<i class="icon-light-down"></i>
				<span class="i-name">icon-light-down</span>
				<span class="i-code">0xe8ab</span>
			</div>
			<div title="Code: 0xe8ac" class="large-16 medium-25 small-33">
				<i class="icon-light-up"></i>
				<span class="i-name">icon-light-up</span>
				<span class="i-code">0xe8ac</span>
			</div>
			<div title="Code: 0xe8ad" class="large-16 medium-25 small-33">
				<i class="icon-adjust"></i>
				<span class="i-name">icon-adjust</span>
				<span class="i-code">0xe8ad</span>
			</div>
			<div title="Code: 0xe8ae" class="large-16 medium-25 small-33">
				<i class="icon-block"></i>
				<span class="i-name">icon-block</span>
				<span class="i-code">0xe8ae</span>
			</div>

			<div title="Code: 0xe8b1" class="large-16 medium-25 small-33">
				<i class="icon-popup"></i>
				<span class="i-name">icon-popup</span>
				<span class="i-code">0xe8b1</span>
			</div>
			<div title="Code: 0xe8b2" class="large-16 medium-25 small-33">
				<i class="icon-publish"></i>
				<span class="i-name">icon-publish</span>
				<span class="i-code">0xe8b2</span>
			</div>
			<div title="Code: 0xe8b3" class="large-16 medium-25 small-33">
				<i class="icon-window"></i>
				<span class="i-name">icon-window</span>
				<span class="i-code">0xe8b3</span>
			</div>



			<div title="Code: 0xe8e9" class="large-16 medium-25 small-33">
				<i class="icon-target"></i>
				<span class="i-name">icon-target</span>
				<span class="i-code">0xe8e9</span>
			</div>
			<div title="Code: 0xe8ea" class="large-16 medium-25 small-33">
				<i class="icon-palette"></i>
				<span class="i-name">icon-palette</span>
				<span class="i-code">0xe8ea</span>
			</div>
			<div title="Code: 0xe8ed" class="large-16 medium-25 small-33">
				<i class="icon-signal"></i>
				<span class="i-name">icon-signal</span>
				<span class="i-code">0xe8ed</span>
			</div>

			<div title="Code: 0xe8ee" class="large-16 medium-25 small-33">
				<i class="icon-trophy"></i>
				<span class="i-name">icon-trophy</span>
				<span class="i-code">0xe8ee</span>
			</div>
			<div title="Code: 0xe8ef" class="large-16 medium-25 small-33">
				<i class="icon-battery"></i>
				<span class="i-name">icon-battery</span>
				<span class="i-code">0xe8ef</span>
			</div>

			<div title="Code: 0xe8f3" class="large-16 medium-25 small-33">
				<i class="icon-network"></i>
				<span class="i-name">icon-network</span>
				<span class="i-code">0xe8f3</span>
			</div>

			<div title="Code: 0xe8f4" class="large-16 medium-25 small-33">
				<i class="icon-cd"></i>
				<span class="i-name">icon-cd</span>
				<span class="i-code">0xe8f4</span>
			</div>
			<div title="Code: 0xe8f5" class="large-16 medium-25 small-33">
				<i class="icon-inbox"></i>
				<span class="i-name">icon-inbox</span>
				<span class="i-code">0xe8f5</span>
			</div>
			<div title="Code: 0xe8f6" class="large-16 medium-25 small-33">
				<i class="icon-install"></i>
				<span class="i-name">icon-install</span>
				<span class="i-code">0xe8f6</span>
			</div>
			<div title="Code: 0xe8f7" class="large-16 medium-25 small-33">
				<i class="icon-globe"></i>
				<span class="i-name">icon-globe</span>
				<span class="i-code">0xe8f7</span>
			</div>

			<div title="Code: 0xe800" class="large-16 medium-25 small-33">
				<i class="icon-cloud-thunder"></i>
				<span class="i-name">icon-cloud-thunder</span>
				<span class="i-code">0xe800</span>
			</div>
			<div title="Code: 0xe8fa" class="large-16 medium-25 small-33">
				<i class="icon-flash"></i>
				<span class="i-name">icon-flash</span>
				<span class="i-code">0xe8fa</span>
			</div>
			<div title="Code: 0xe8fb" class="large-16 medium-25 small-33">
				<i class="icon-moon"></i>
				<span class="i-name">icon-moon</span>
				<span class="i-code">0xe8fb</span>
			</div>
			<div title="Code: 0xe8fc" class="large-16 medium-25 small-33">
				<i class="icon-flight"></i>
				<span class="i-name">icon-flight</span>
				<span class="i-code">0xe8fc</span>
			</div>

			<div title="Code: 0xe8fd" class="large-16 medium-25 small-33">
				<i class="icon-paper-plane"></i>
				<span class="i-name">icon-paper-plane</span>
				<span class="i-code">0xe8fd</span>
			</div>
			<div title="Code: 0xe8fe" class="large-16 medium-25 small-33">
				<i class="icon-leaf"></i>
				<span class="i-name">icon-leaf</span>
				<span class="i-code">0xe8fe</span>
			</div>
			<div title="Code: 0xe8ff" class="large-16 medium-25 small-33">
				<i class="icon-lifebuoy"></i>
				<span class="i-name">icon-lifebuoy</span>
				<span class="i-code">0xe8ff</span>
			</div>
			<div title="Code: 0xe900" class="large-16 medium-25 small-33">
				<i class="icon-mouse"></i>
				<span class="i-name">icon-mouse</span>
				<span class="i-code">0xe900</span>
			</div>

			<div title="Code: 0xe901" class="large-16 medium-25 small-33">
				<i class="icon-briefcase"></i>
				<span class="i-name">icon-briefcase</span>
				<span class="i-code">0xe901</span>
			</div>
			<div title="Code: 0xe902" class="large-16 medium-25 small-33">
				<i class="icon-suitcase"></i>
				<span class="i-name">icon-suitcase</span>
				<span class="i-code">0xe902</span>
			</div>
			<div title="Code: 0xe903" class="large-16 medium-25 small-33">
				<i class="icon-dot-1"></i>
				<span class="i-name">icon-dot-1</span>
				<span class="i-code">0xe903</span>
			</div>
			<div title="Code: 0xe904" class="large-16 medium-25 small-33">
				<i class="icon-dot-2"></i>
				<span class="i-name">icon-dot-2</span>
				<span class="i-code">0xe904</span>
			</div>

			<div title="Code: 0xe905" class="large-16 medium-25 small-33">
				<i class="icon-dot-3"></i>
				<span class="i-name">icon-dot-3</span>
				<span class="i-code">0xe905</span>
			</div>
			<div title="Code: 0xe906" class="large-16 medium-25 small-33">
				<i class="icon-brush"></i>
				<span class="i-name">icon-brush</span>
				<span class="i-code">0xe906</span>
			</div>
			<div title="Code: 0xe907" class="large-16 medium-25 small-33">
				<i class="icon-magnet-1"></i>
				<span class="i-name">icon-magnet-1</span>
				<span class="i-code">0xe907</span>
			</div>
			<div title="Code: 0xe908" class="large-16 medium-25 small-33">
				<i class="icon-infinity"></i>
				<span class="i-name">icon-infinity</span>
				<span class="i-code">0xe908</span>
			</div>

			<div title="Code: 0xe909" class="large-16 medium-25 small-33">
				<i class="icon-erase"></i>
				<span class="i-name">icon-erase</span>
				<span class="i-code">0xe909</span>
			</div>
			<div title="Code: 0xe90a" class="large-16 medium-25 small-33">
				<i class="icon-chart-pie"></i>
				<span class="i-name">icon-chart-pie</span>
				<span class="i-code">0xe90a</span>
			</div>
			<div title="Code: 0xe90b" class="large-16 medium-25 small-33">
				<i class="icon-chart-line"></i>
				<span class="i-name">icon-chart-line</span>
				<span class="i-code">0xe90b</span>
			</div>
			<div title="Code: 0xe90c" class="large-16 medium-25 small-33">
				<i class="icon-chart-bar"></i>
				<span class="i-name">icon-chart-bar</span>
				<span class="i-code">0xe90c</span>
			</div>

			<div title="Code: 0xe90d" class="large-16 medium-25 small-33">
				<i class="icon-chart-area"></i>
				<span class="i-name">icon-chart-area</span>
				<span class="i-code">0xe90d</span>
			</div>
			<div title="Code: 0xe90e" class="large-16 medium-25 small-33">
				<i class="icon-tape"></i>
				<span class="i-name">icon-tape</span>
				<span class="i-code">0xe90e</span>
			</div>
			<div title="Code: 0xe90f" class="large-16 medium-25 small-33">
				<i class="icon-graduation-cap"></i>
				<span class="i-name">icon-graduation-cap</span>
				<span class="i-code">0xe90f</span>
			</div>
			<div title="Code: 0xe911" class="large-16 medium-25 small-33">
				<i class="icon-ticket"></i>
				<span class="i-name">icon-ticket</span>
				<span class="i-code">0xe911</span>
			</div>

			<div title="Code: 0xe912" class="large-16 medium-25 small-33">
				<i class="icon-water"></i>
				<span class="i-name">icon-water</span>
				<span class="i-code">0xe912</span>
			</div>
			<div title="Code: 0xe913" class="large-16 medium-25 small-33">
				<i class="icon-droplet"></i>
				<span class="i-name">icon-droplet</span>
				<span class="i-code">0xe913</span>
			</div>
			<div title="Code: 0xe914" class="large-16 medium-25 small-33">
				<i class="icon-air"></i>
				<span class="i-name">icon-air</span>
				<span class="i-code">0xe914</span>
			</div>
			<div title="Code: 0xe915" class="large-16 medium-25 small-33">
				<i class="icon-credit-card"></i>
				<span class="i-name">icon-credit-card</span>
				<span class="i-code">0xe915</span>
			</div>

			<div title="Code: 0xe916" class="large-16 medium-25 small-33">
				<i class="icon-floppy"></i>
				<span class="i-name">icon-floppy</span>
				<span class="i-code">0xe916</span>
			</div>
			<div title="Code: 0xe917" class="large-16 medium-25 small-33">
				<i class="icon-clipboard"></i>
				<span class="i-name">icon-clipboard</span>
				<span class="i-code">0xe917</span>
			</div>
			<div title="Code: 0xe918" class="large-16 medium-25 small-33">
				<i class="icon-megaphone"></i>
				<span class="i-name">icon-megaphone</span>
				<span class="i-code">0xe918</span>
			</div>
			<div title="Code: 0xe919" class="large-16 medium-25 small-33">
				<i class="icon-database"></i>
				<span class="i-name">icon-database</span>
				<span class="i-code">0xe919</span>
			</div>

			<div title="Code: 0xe91a" class="large-16 medium-25 small-33">
				<i class="icon-drive"></i>
				<span class="i-name">icon-drive</span>
				<span class="i-code">0xe91a</span>
			</div>
			<div title="Code: 0xe91b" class="large-16 medium-25 small-33">
				<i class="icon-bucket"></i>
				<span class="i-name">icon-bucket</span>
				<span class="i-code">0xe91b</span>
			</div>

			<div title="Code: 0xe91d" class="large-16 medium-25 small-33">
				<i class="icon-key"></i>
				<span class="i-name">icon-key</span>
				<span class="i-code">0xe91d</span>
			</div>
			<div title="Code: 0xe95f" class="large-16 medium-25 small-33">
				<i class="icon-sweden"></i>
				<span class="i-name">icon-sweden</span>
				<span class="i-code">0xe95f</span>
			</div>
			<!-- Control Icons -->
			<h3>Control Icons</h3>
			<div title="Code: 0xe96f" class="large-16 medium-25 small-33">
				<i class="icon-check-1"></i>
				<span class="i-name">icon-check-1</span>
				<span class="i-code">0xe96f</span>
			</div>
			<div title="Code: 0xe970" class="large-16 medium-25 small-33">
				<i class="icon-check-empty"></i>
				<span class="i-name">icon-check-empty</span>
				<span class="i-code">0xe970</span>
			</div>
			<div title="Code: 0xe841" class="large-16 medium-25 small-33">
				<i class="icon-minus"></i>
				<span class="i-name">icon-minus</span>
				<span class="i-code">0xe841</span>
			</div>
			<div title="Code: 0xe842" class="large-16 medium-25 small-33">
				<i class="icon-minus-circled"></i>
				<span class="i-name">icon-minus-circled</span>
				<span class="i-code">0xe842</span>
			</div>

			<div title="Code: 0xe843" class="large-16 medium-25 small-33">
				<i class="icon-minus-squared"></i>
				<span class="i-name">icon-minus-squared</span>
				<span class="i-code">0xe843</span>
			</div>
			<div title="Code: 0xe839" class="large-16 medium-25 small-33">
				<i class="icon-menu-1"></i>
				<span class="i-name">icon-menu-1</span>
				<span class="i-code">0xe839</span>
			</div>
			<div title="Code: 0xe83a" class="large-16 medium-25 small-33">
				<i class="icon-check"></i>
				<span class="i-name">icon-check</span>
				<span class="i-code">0xe83a</span>
			</div>
			<div title="Code: 0xea2a" class="large-16 medium-25 small-33">
				<i class="icon-mail"></i>
				<span class="i-name">icon-mail</span>
				<span class="i-code">0xea2a</span>
			</div>
			<div title="Code: 0xe83c" class="large-16 medium-25 small-33">
				<i class="icon-cancel-circled"></i>
				<span class="i-name">icon-cancel-circled</span>
				<span class="i-code">0xe83c</span>
			</div>
			<div title="Code: 0xe83d" class="large-16 medium-25 small-33">
				<i class="icon-cancel-squared"></i>
				<span class="i-name">icon-cancel-squared</span>
				<span class="i-code">0xe83d</span>
			</div>
			<div title="Code: 0xe83e" class="large-16 medium-25 small-33">
				<i class="icon-plus"></i>
				<span class="i-name">icon-plus</span>
				<span class="i-code">0xe83e</span>
			</div>

			<div title="Code: 0xe83f" class="large-16 medium-25 small-33">
				<i class="icon-plus-circled"></i>
				<span class="i-name">icon-plus-circled</span>
				<span class="i-code">0xe83f</span>
			</div>
			<div title="Code: 0xe840" class="large-16 medium-25 small-33">
				<i class="icon-plus-squared"></i>
				<span class="i-name">icon-plus-squared</span>
				<span class="i-code">0xe840</span>
			</div>
			<div title="Code: 0xe8dd" class="large-16 medium-25 small-33">
				<i class="icon-play"></i>
				<span class="i-name">icon-play</span>
				<span class="i-code">0xe8dd</span>
			</div>
			<div title="Code: 0xe8de" class="large-16 medium-25 small-33">
				<i class="icon-stop"></i>
				<span class="i-name">icon-stop</span>
				<span class="i-code">0xe8de</span>
			</div>
			<div title="Code: 0xe8df" class="large-16 medium-25 small-33">
				<i class="icon-pause"></i>
				<span class="i-name">icon-pause</span>
				<span class="i-code">0xe8df</span>
			</div>

			<div title="Code: 0xe8e0" class="large-16 medium-25 small-33">
				<i class="icon-record"></i>
				<span class="i-name">icon-record</span>
				<span class="i-code">0xe8e0</span>
			</div>
			<div title="Code: 0xe8e1" class="large-16 medium-25 small-33">
				<i class="icon-to-end"></i>
				<span class="i-name">icon-to-end</span>
				<span class="i-code">0xe8e1</span>
			</div>
			<div title="Code: 0xe8e2" class="large-16 medium-25 small-33">
				<i class="icon-to-start-1"></i>
				<span class="i-name">icon-to-start-1</span>
				<span class="i-code">0xe8e2</span>
			</div>
			<div title="Code: 0xe8e3" class="large-16 medium-25 small-33">
				<i class="icon-fast-forward"></i>
				<span class="i-name">icon-fast-forward</span>
				<span class="i-code">0xe8e3</span>
			</div>

			<div title="Code: 0xe8e4" class="large-16 medium-25 small-33">
				<i class="icon-fast-backward"></i>
				<span class="i-name">icon-fast-backward</span>
				<span class="i-code">0xe8e4</span>
			</div>
			<div title="Code: 0xe8e5" class="large-16 medium-25 small-33">
				<i class="icon-progress-0"></i>
				<span class="i-name">icon-progress-0</span>
				<span class="i-code">0xe8e5</span>
			</div>
			<div title="Code: 0xe8e6" class="large-16 medium-25 small-33">
				<i class="icon-progress-1"></i>
				<span class="i-name">icon-progress-1</span>
				<span class="i-code">0xe8e6</span>
			</div>
			<div title="Code: 0xe8e7" class="large-16 medium-25 small-33">
				<i class="icon-progress-2"></i>
				<span class="i-name">icon-progress-2</span>
				<span class="i-code">0xe8e7</span>
			</div>

			<div title="Code: 0xe8e8" class="large-16 medium-25 small-33">
				<i class="icon-progress-3"></i>
				<span class="i-name">icon-progress-3</span>
				<span class="i-code">0xe8e8</span>
			</div>
			<!-- Text Editor Icons -->
			<h3>Text Editor Icons</h3>
			<div title="Code: 0xe88d" class="large-16 medium-25 small-33">
				<i class="icon-trash-1"></i>
				<span class="i-name">icon-trash-1</span>
				<span class="i-code">0xe88d</span>
			</div>
			<div title="Code: 0xe88e" class="large-16 medium-25 small-33">
				<i class="icon-doc-1"></i>
				<span class="i-name">icon-doc-1</span>
				<span class="i-code">0xe88e</span>
			</div>
			<div title="Code: 0xe88f" class="large-16 medium-25 small-33">
				<i class="icon-docs-1"></i>
				<span class="i-name">icon-docs-1</span>
				<span class="i-code">0xe88f</span>
			</div>
			<div title="Code: 0xe890" class="large-16 medium-25 small-33">
				<i class="icon-doc-landscape"></i>
				<span class="i-name">icon-doc-landscape</span>
				<span class="i-code">0xe890</span>
			</div>
			<div title="Code: 0xe891" class="large-16 medium-25 small-33">
				<i class="icon-doc-text"></i>
				<span class="i-name">icon-doc-text</span>
				<span class="i-code">0xe891</span>
			</div>
			<div title="Code: 0xe892" class="large-16 medium-25 small-33">
				<i class="icon-doc-text-inv"></i>
				<span class="i-name">icon-doc-text-inv</span>
				<span class="i-code">0xe892</span>
			</div>
			<div title="Code: 0xe893" class="large-16 medium-25 small-33">
				<i class="icon-newspaper"></i>
				<span class="i-name">icon-newspaper</span>
				<span class="i-code">0xe893</span>
			</div>
			<div title="Code: 0xe894" class="large-16 medium-25 small-33">
				<i class="icon-book-open"></i>
				<span class="i-name">icon-book-open</span>
				<span class="i-code">0xe894</span>
			</div>
			<div title="Code: 0xe895" class="large-16 medium-25 small-33">
				<i class="icon-book"></i>
				<span class="i-name">icon-book</span>
				<span class="i-code">0xe895</span>
			</div>
			<div title="Code: 0xe896" class="large-16 medium-25 small-33">
				<i class="icon-folder"></i>
				<span class="i-name">icon-folder</span>
				<span class="i-code">0xe896</span>
			</div>
			<div title="Code: 0xe897" class="large-16 medium-25 small-33">
				<i class="icon-archive"></i>
				<span class="i-name">icon-archive</span>
				<span class="i-code">0xe897</span>
			</div>
			<div title="Code: 0xe898" class="large-16 medium-25 small-33">
				<i class="icon-box"></i>
				<span class="i-name">icon-box</span>
				<span class="i-code">0xe898</span>
			</div>
			<div title="Code: 0xea88" class="large-16 medium-25 small-33">
				<i class="icon-export-1"></i>
				<span class="i-name">icon-export-1</span>
				<span class="i-code">0xea88</span>
			</div>
			<div title="Code: 0xe86e" class="large-16 medium-25 small-33">
				<i class="icon-export-alt"></i>
				<span class="i-name">icon-export-alt</span>
				<span class="i-code">0xe86e</span>
			</div>
			<div title="Code: 0xe86f" class="large-16 medium-25 small-33">
				<i class="icon-pencil-1"></i>
				<span class="i-name">icon-pencil-1</span>
				<span class="i-code">0xe86f</span>
			</div>
			<div title="Code: 0xe866" class="large-16 medium-25 small-33">
				<i class="icon-pencil-squared"></i>
				<span class="i-name">icon-pencil-squared</span>
				<span class="i-code">0xe866</span>
			</div>
			<div title="Code: 0xea53" class="large-16 medium-25 small-33">
				<i class="icon-edit"></i>
				<span class="i-name">icon-edit</span>
				<span class="i-code">0xea53</span>
			</div>
			<div title="Code: 0xea54" class="large-16 medium-25 small-33">
				<i class="icon-print-1"></i>
				<span class="i-name">icon-print-1</span>
				<span class="i-code">0xea54</span>
			</div>
			<div title="Code: 0xe871" class="large-16 medium-25 small-33">
				<i class="icon-trash"></i>
				<span class="i-name">icon-trash</span>
				<span class="i-code">0xe871</span>
			</div>
			<div title="Code: 0xe870" class="large-16 medium-25 small-33">
				<i class="icon-doc"></i>
				<span class="i-name">icon-doc</span>
				<span class="i-code">0xe870</span>
			</div>
			<div title="Code: 0xe865" class="large-16 medium-25 small-33">
				<i class="icon-docs"></i>
				<span class="i-name">icon-docs</span>
				<span class="i-code">0xe865</span>
			</div>
			<div title="Code: 0xea5f" class="large-16 medium-25 small-33">
				<i class="icon-doc-text-1"></i>
				<span class="i-name">icon-doc-text-1</span>
				<span class="i-code">0xea5f</span>
			</div>
			<div title="Code: 0xea60" class="large-16 medium-25 small-33">
				<i class="icon-doc-inv"></i>
				<span class="i-name">icon-doc-inv</span>
				<span class="i-code">0xea60</span>
			</div>
			<div title="Code: 0xea61" class="large-16 medium-25 small-33">
				<i class="icon-doc-text-inv-1"></i>
				<span class="i-name">icon-doc-text-inv-1</span>
				<span class="i-code">0xea61</span>
			</div>
			<div title="Code: 0xea62" class="large-16 medium-25 small-33">
				<i class="icon-folder-1"></i>
				<span class="i-name">icon-folder-1</span>
				<span class="i-code">0xea62</span>
			</div>
			<div title="Code: 0xea63" class="large-16 medium-25 small-33">
				<i class="icon-folder-open"></i>
				<span class="i-name">icon-folder-open</span>
				<span class="i-code">0xea63</span>
			</div>
			<div title="Code: 0xea64" class="large-16 medium-25 small-33">
				<i class="icon-folder-empty"></i>
				<span class="i-name">icon-folder-empty</span>
				<span class="i-code">0xea64</span>
			</div>
			<div title="Code: 0xea65" class="large-16 medium-25 small-33">
				<i class="icon-folder-open-empty"></i>
				<span class="i-name">icon-folder-open-empty</span>
				<span class="i-code">0xea65</span>
			</div>
			<div title="Code: 0xea66" class="large-16 medium-25 small-33">
				<i class="icon-box-1"></i>
				<span class="i-name">icon-box-1</span>
				<span class="i-code">0xea66</span>
			</div>
			<div title="Code: 0xe820" class="large-16 medium-25 small-33">
				<i class="icon-font"></i>
				<span class="i-name">icon-font</span>
				<span class="i-code">0xe820</span>
			</div>
			<div title="Code: 0xe821" class="large-16 medium-25 small-33">
				<i class="icon-bold"></i>
				<span class="i-name">icon-bold</span>
				<span class="i-code">0xe821</span>
			</div>
			<div title="Code: 0xe822" class="large-16 medium-25 small-33">
				<i class="icon-italic"></i>
				<span class="i-name">icon-italic</span>
				<span class="i-code">0xe822</span>
			</div>
			<div title="Code: 0xe9e4" class="large-16 medium-25 small-33">
				<i class="icon-text-height"></i>
				<span class="i-name">icon-text-height</span>
				<span class="i-code">0xe9e4</span>
			</div>
			<div title="Code: 0xe9ed" class="large-16 medium-25 small-33">
				<i class="icon-text-width"></i>
				<span class="i-name">icon-text-width</span>
				<span class="i-code">0xe9ed</span>
			</div>
			<div title="Code: 0xea12" class="large-16 medium-25 small-33">
				<i class="icon-align-left"></i>
				<span class="i-name">icon-align-left</span>
				<span class="i-code">0xea12</span>
			</div>
			<div title="Code: 0xea13" class="large-16 medium-25 small-33">
				<i class="icon-align-center"></i>
				<span class="i-name">icon-align-center</span>
				<span class="i-code">0xea13</span>
			</div>
			<div title="Code: 0xea14" class="large-16 medium-25 small-33">
				<i class="icon-align-right"></i>
				<span class="i-name">icon-align-right</span>
				<span class="i-code">0xea14</span>
			</div>
			<div title="Code: 0xea15" class="large-16 medium-25 small-33">
				<i class="icon-align-justify"></i>
				<span class="i-name">icon-align-justify</span>
				<span class="i-code">0xea15</span>
			</div>
			<div title="Code: 0xea16" class="large-16 medium-25 small-33">
				<i class="icon-list-1"></i>
				<span class="i-name">icon-list-1</span>
				<span class="i-code">0xea16</span>
			</div>
			<div title="Code: 0xea17" class="large-16 medium-25 small-33">
				<i class="icon-indent-left"></i>
				<span class="i-name">icon-indent-left</span>
				<span class="i-code">0xea17</span>
			</div>
			<div title="Code: 0xea18" class="large-16 medium-25 small-33">
				<i class="icon-indent-right"></i>
				<span class="i-name">icon-indent-right</span>
				<span class="i-code">0xea18</span>
			</div>
			<div title="Code: 0xea19" class="large-16 medium-25 small-33">
				<i class="icon-list-bullet"></i>
				<span class="i-name">icon-list-bullet</span>
				<span class="i-code">0xea19</span>
			</div>
			<div title="Code: 0xea1a" class="large-16 medium-25 small-33">
				<i class="icon-list-numbered"></i>
				<span class="i-name">icon-list-numbered</span>
				<span class="i-code">0xea1a</span>
			</div>
			<div title="Code: 0xea1b" class="large-16 medium-25 small-33">
				<i class="icon-strike"></i>
				<span class="i-name">icon-strike</span>
				<span class="i-code">0xea1b</span>
			</div>
			<div title="Code: 0xea1c" class="large-16 medium-25 small-33">
				<i class="icon-underline"></i>
				<span class="i-name">icon-underline</span>
				<span class="i-code">0xea1c</span>
			</div>
			<div title="Code: 0xea1d" class="large-16 medium-25 small-33">
				<i class="icon-superscript"></i>
				<span class="i-name">icon-superscript</span>
				<span class="i-code">0xea1d</span>
			</div>
			<div title="Code: 0xe823" class="large-16 medium-25 small-33">
				<i class="icon-subscript"></i>
				<span class="i-name">icon-subscript</span>
				<span class="i-code">0xe823</span>
			</div>
			<div title="Code: 0xe824" class="large-16 medium-25 small-33">
				<i class="icon-table"></i>
				<span class="i-name">icon-table</span>
				<span class="i-code">0xe824</span>
			</div>
			<div title="Code: 0xe825" class="large-16 medium-25 small-33">
				<i class="icon-columns"></i>
				<span class="i-name">icon-columns</span>
				<span class="i-code">0xe825</span>
			</div>
			<div title="Code: 0xe826" class="large-16 medium-25 small-33">
				<i class="icon-crop"></i>
				<span class="i-name">icon-crop</span>
				<span class="i-code">0xe826</span>
			</div>
			<div title="Code: 0xe827" class="large-16 medium-25 small-33">
				<i class="icon-scissors"></i>
				<span class="i-name">icon-scissors</span>
				<span class="i-code">0xe827</span>
			</div>
			<div title="Code: 0xe962" class="large-16 medium-25 small-33">
				<i class="icon-paste"></i>
				<span class="i-name">icon-paste</span>
				<span class="i-code">0xe962</span>
			</div>
			<!-- Icon Directional -->
			<h3>Directional Icons</h3>
			<div title="Code: 0xea51" class="large-16 medium-25 small-33">
				<i class="icon-reply-1"></i>
				<span class="i-name">icon-reply-1</span>
				<span class="i-code">0xea51</span>
			</div>
			<div title="Code: 0xe8b0" class="large-16 medium-25 small-33">
				<i class="icon-resize-small-1"></i>
				<span class="i-name">icon-resize-small-1</span>
				<span class="i-code">0xe8b0</span>
			</div>
			<div title="Code: 0xe8af" class="large-16 medium-25 small-33">
				<i class="icon-resize-full-1"></i>
				<span class="i-name">icon-resize-full-1</span>
				<span class="i-code">0xe8af</span>
			</div>
			<div title="Code: 0xea52" class="large-16 medium-25 small-33">
				<i class="icon-reply-all-1"></i>
				<span class="i-name">icon-reply-all-1</span>
				<span class="i-code">0xea52</span>
			</div>
			<div title="Code: 0xea78" class="large-16 medium-25 small-33">
				<i class="icon-forward-1"></i>
				<span class="i-name">icon-forward-1</span>
				<span class="i-code">0xea78</span>
			</div>
			<div title="Code: 0xe992" class="large-16 medium-25 small-33">
				<i class="icon-sort"></i>
				<span class="i-name">icon-sort</span>
				<span class="i-code">0xe992</span>
			</div>

			<div title="Code: 0xe995" class="large-16 medium-25 small-33">
				<i class="icon-sort-alt-up"></i>
				<span class="i-name">icon-sort-alt-up</span>
				<span class="i-code">0xe995</span>
			</div>
			<div title="Code: 0xe996" class="large-16 medium-25 small-33">
				<i class="icon-sort-alt-down"></i>
				<span class="i-name">icon-sort-alt-down</span>
				<span class="i-code">0xe996</span>
			</div>

			<div title="Code: 0xe997" class="large-16 medium-25 small-33">
				<i class="icon-sort-name-up"></i>
				<span class="i-name">icon-sort-name-up</span>
				<span class="i-code">0xe997</span>
			</div>
			<div title="Code: 0xe998" class="large-16 medium-25 small-33">
				<i class="icon-sort-name-down"></i>
				<span class="i-name">icon-sort-name-down</span>
				<span class="i-code">0xe998</span>
			</div>
			<div title="Code: 0xe999" class="large-16 medium-25 small-33">
				<i class="icon-sort-number-up"></i>
				<span class="i-name">icon-sort-number-up</span>
				<span class="i-code">0xe999</span>
			</div>
			<div title="Code: 0xe99a" class="large-16 medium-25 small-33">
				<i class="icon-sort-number-down"></i>
				<span class="i-name">icon-sort-number-down</span>
				<span class="i-code">0xe99a</span>
			</div>
			<div title="Code: 0xe993" class="large-16 medium-25 small-33">
				<i class="icon-sort-down"></i>
				<span class="i-name">icon-sort-down</span>
				<span class="i-code">0xe993</span>
			</div>
			<div title="Code: 0xe994" class="large-16 medium-25 small-33">
				<i class="icon-sort-up"></i>
				<span class="i-name">icon-sort-up</span>
				<span class="i-code">0xe994</span>
			</div>
			<div title="Code: 0xe8f0" class="large-16 medium-25 small-33">
				<i class="icon-back-in-time"></i>
				<span class="i-name">icon-back-in-time</span>
				<span class="i-code">0xe8f0</span>
			</div>                                                                  
			<div title="Code: 0xe8b4" class="large-16 medium-25 small-33">
				<i class="icon-arrow-combo"></i>
				<span class="i-name">icon-arrow-combo</span>
				<span class="i-code">0xe8b4</span>
			</div>
			<div title="Code: 0xe8b5" class="large-16 medium-25 small-33">
				<i class="icon-down-circled-1"></i>
				<span class="i-name">icon-down-circled-1</span>
				<span class="i-code">0xe8b5</span>
			</div>
			<div title="Code: 0xe8b6" class="large-16 medium-25 small-33">
				<i class="icon-left-circled-2"></i>
				<span class="i-name">icon-left-circled-2</span>
				<span class="i-code">0xe8b6</span>
			</div>
			<div title="Code: 0xe8b7" class="large-16 medium-25 small-33">
				<i class="icon-right-circled-1"></i>
				<span class="i-name">icon-right-circled-1</span>
				<span class="i-code">0xe8b7</span>
			</div>
			<div title="Code: 0xe8b8" class="large-16 medium-25 small-33">
				<i class="icon-up-circled-1"></i>
				<span class="i-name">icon-up-circled-1</span>
				<span class="i-code">0xe8b8</span>
			</div>
			<div title="Code: 0xe86a" class="large-16 medium-25 small-33">
				<i class="icon-down-open-big"></i>
				<span class="i-name">icon-down-open-big</span>
				<span class="i-code">0xe86a</span>
			</div>
			<div title="Code: 0xe84d" class="large-16 medium-25 small-33">
				<i class="icon-left-open-big"></i>
				<span class="i-name">icon-left-open-big</span>
				<span class="i-code">0xe84d</span>
			</div>
			<div title="Code: 0xe84c" class="large-16 medium-25 small-33">
				<i class="icon-right-open-big"></i>
				<span class="i-name">icon-right-open-big</span>
				<span class="i-code">0xe84c</span>
			</div>

			<div title="Code: 0xe8c5" class="large-16 medium-25 small-33">
				<i class="icon-up-open-big"></i>
				<span class="i-name">icon-up-open-big</span>
				<span class="i-code">0xe8c5</span>
			</div>
			<div title="Code: 0xe8c9" class="large-16 medium-25 small-33">
				<i class="icon-down-dir-1"></i>
				<span class="i-name">icon-down-dir-1</span>
				<span class="i-code">0xe8c9</span>
			</div>
			<div title="Code: 0xe8ca" class="large-16 medium-25 small-33">
				<i class="icon-left-dir-1"></i>
				<span class="i-name">icon-left-dir-1</span>
				<span class="i-code">0xe8ca</span>
			</div>
			<div title="Code: 0xe8cb" class="large-16 medium-25 small-33">
				<i class="icon-right-dir-1"></i>
				<span class="i-name">icon-right-dir-1</span>
				<span class="i-code">0xe8cb</span>
			</div>

			<div title="Code: 0xe8cc" class="large-16 medium-25 small-33">
				<i class="icon-up-dir-1"></i>
				<span class="i-name">icon-up-dir-1</span>
				<span class="i-code">0xe8cc</span>
			</div>
			<div title="Code: 0xe8cd" class="large-16 medium-25 small-33">
				<i class="icon-down-bold"></i>
				<span class="i-name">icon-down-bold</span>
				<span class="i-code">0xe8cd</span>
			</div>
			<div title="Code: 0xe8ce" class="large-16 medium-25 small-33">
				<i class="icon-left-bold"></i>
				<span class="i-name">icon-left-bold</span>
				<span class="i-code">0xe8ce</span>
			</div>
			<div title="Code: 0xe8cf" class="large-16 medium-25 small-33">
				<i class="icon-right-bold"></i>
				<span class="i-name">icon-right-bold</span>
				<span class="i-code">0xe8cf</span>
			</div>

			<div title="Code: 0xe8d0" class="large-16 medium-25 small-33">
				<i class="icon-up-bold"></i>
				<span class="i-name">icon-up-bold</span>
				<span class="i-code">0xe8d0</span>
			</div>
			<div title="Code: 0xe8d5" class="large-16 medium-25 small-33">
				<i class="icon-ccw-1"></i>
				<span class="i-name">icon-ccw-1</span>
				<span class="i-code">0xe8d5</span>
			</div>
			<div title="Code: 0xe8d6" class="large-16 medium-25 small-33">
				<i class="icon-cw-1"></i>
				<span class="i-name">icon-cw-1</span>
				<span class="i-code">0xe8d6</span>
			</div>
			<div title="Code: 0xe8d7" class="large-16 medium-25 small-33">
				<i class="icon-arrows-ccw"></i>
				<span class="i-name">icon-arrows-ccw</span>
				<span class="i-code">0xe8d7</span>
			</div>

			<div title="Code: 0xe8d8" class="large-16 medium-25 small-33">
				<i class="icon-level-down-1"></i>
				<span class="i-name">icon-level-down-1</span>
				<span class="i-code">0xe8d8</span>
			</div>
			<div title="Code: 0xe8d9" class="large-16 medium-25 small-33">
				<i class="icon-level-up-1"></i>
				<span class="i-name">icon-level-up-1</span>
				<span class="i-code">0xe8d9</span>
			</div>
			<div title="Code: 0xe8da" class="large-16 medium-25 small-33">
				<i class="icon-shuffle-1"></i>
				<span class="i-name">icon-shuffle-1</span>
				<span class="i-code">0xe8da</span>
			</div>
			<div title="Code: 0xe8db" class="large-16 medium-25 small-33">
				<i class="icon-loop"></i>
				<span class="i-name">icon-loop</span>
				<span class="i-code">0xe8db</span>
			</div>

			<div title="Code: 0xe8dc" class="large-16 medium-25 small-33">
				<i class="icon-switch"></i>
				<span class="i-name">icon-switch</span>
				<span class="i-code">0xe8dc</span>
			</div>
			<div title="Code: 0xea75" class="large-16 medium-25 small-33">
				<i class="icon-resize-full"></i>
				<span class="i-name">icon-resize-full</span>
				<span class="i-code">0xea75</span>
			</div>
			<div title="Code: 0xea76" class="large-16 medium-25 small-33">
				<i class="icon-resize-full-alt"></i>
				<span class="i-name">icon-resize-full-alt</span>
				<span class="i-code">0xea76</span>
			</div>
			<div title="Code: 0xea23" class="large-16 medium-25 small-33">
				<i class="icon-resize-small"></i>
				<span class="i-name">icon-resize-small</span>
				<span class="i-code">0xea23</span>
			</div>
			<div title="Code: 0xea22" class="large-16 medium-25 small-33">
				<i class="icon-resize-vertical"></i>
				<span class="i-name">icon-resize-vertical</span>
				<span class="i-code">0xea22</span>
			</div>
			<div title="Code: 0xea21" class="large-16 medium-25 small-33">
				<i class="icon-resize-horizontal"></i>
				<span class="i-name">icon-resize-horizontal</span>
				<span class="i-code">0xea21</span>
			</div>
			<div title="Code: 0xe858" class="large-16 medium-25 small-33">
				<i class="icon-move"></i>
				<span class="i-name">icon-move</span>
				<span class="i-code">0xe858</span>
			</div>
			<div title="Code: 0xe857" class="large-16 medium-25 small-33">
				<i class="icon-zoom-in"></i>
				<span class="i-name">icon-zoom-in</span>
				<span class="i-code">0xe857</span>
			</div>
			<div title="Code: 0xe856" class="large-16 medium-25 small-33">
				<i class="icon-zoom-out"></i>
				<span class="i-name">icon-zoom-out</span>
				<span class="i-code">0xe856</span>
			</div>
			<div title="Code: 0xe855" class="large-16 medium-25 small-33">
				<i class="icon-down-circled"></i>
				<span class="i-name">icon-down-circled</span>
				<span class="i-code">0xe855</span>
			</div>
			<div title="Code: 0xe854" class="large-16 medium-25 small-33">
				<i class="icon-up-circled2"></i>
				<span class="i-name">icon-up-circled2</span>
				<span class="i-code">0xe854</span>
			</div>
			<div title="Code: 0xe807" class="large-16 medium-25 small-33">
				<i class="icon-left-circled"></i>
				<span class="i-name">icon-left-circled</span>
				<span class="i-code">0xe807</span>
			</div>
			<div title="Code: 0xe808" class="large-16 medium-25 small-33">
				<i class="icon-right-circled2"></i>
				<span class="i-name">icon-right-circled2</span>
				<span class="i-code">0xe808</span>
			</div>
			<div title="Code: 0xe80d" class="large-16 medium-25 small-33">
				<i class="icon-down-open"></i>
				<span class="i-name">icon-down-open</span>
				<span class="i-code">0xe80d</span>
			</div>
			<div title="Code: 0xe80e" class="large-16 medium-25 small-33">
				<i class="icon-left-open"></i>
				<span class="i-name">icon-left-open</span>
				<span class="i-code">0xe80e</span>
			</div>
			<div title="Code: 0xe80f" class="large-16 medium-25 small-33">
				<i class="icon-right-open"></i>
				<span class="i-name">icon-right-open</span>
				<span class="i-code">0xe80f</span>
			</div>
			<div title="Code: 0xe810" class="large-16 medium-25 small-33">
				<i class="icon-up-open"></i>
				<span class="i-name">icon-up-open</span>
				<span class="i-code">0xe810</span>
			</div>
			<div title="Code: 0xea1e" class="large-16 medium-25 small-33">
				<i class="icon-angle-circled-left"></i>
				<span class="i-name">icon-angle-circled-left</span>
				<span class="i-code">0xea1e</span>
			</div>
			<div title="Code: 0xe85f" class="large-16 medium-25 small-33">
				<i class="icon-angle-circled-right"></i>
				<span class="i-name">icon-angle-circled-right</span>
				<span class="i-code">0xe85f</span>
			</div>
			<div title="Code: 0xe860" class="large-16 medium-25 small-33">
				<i class="icon-angle-circled-up"></i>
				<span class="i-name">icon-angle-circled-up</span>
				<span class="i-code">0xe860</span>
			</div>
			<div title="Code: 0xe861" class="large-16 medium-25 small-33">
				<i class="icon-angle-circled-down"></i>
				<span class="i-name">icon-angle-circled-down</span>
				<span class="i-code">0xe861</span>
			</div>
			<div title="Code: 0xe862" class="large-16 medium-25 small-33">
				<i class="icon-angle-double-left"></i>
				<span class="i-name">icon-angle-double-left</span>
				<span class="i-code">0xe862</span>
			</div>
			<div title="Code: 0xe863" class="large-16 medium-25 small-33">
				<i class="icon-angle-double-right"></i>
				<span class="i-name">icon-angle-double-right</span>
				<span class="i-code">0xe863</span>
			</div>
			<div title="Code: 0xe9e7" class="large-16 medium-25 small-33">
				<i class="icon-angle-double-up"></i>
				<span class="i-name">icon-angle-double-up</span>
				<span class="i-code">0xe9e7</span>
			</div>
			<div title="Code: 0xe9e9" class="large-16 medium-25 small-33">
				<i class="icon-angle-double-down"></i>
				<span class="i-name">icon-angle-double-down</span>
				<span class="i-code">0xe9e9</span>
			</div>
			<div title="Code: 0xe9f1" class="large-16 medium-25 small-33">
				<i class="icon-down-big"></i>
				<span class="i-name">icon-down-big</span>
				<span class="i-code">0xe9f1</span>
			</div>
			<div title="Code: 0xe9f2" class="large-16 medium-25 small-33">
				<i class="icon-left-big"></i>
				<span class="i-name">icon-left-big</span>
				<span class="i-code">0xe9f2</span>
			</div>
			<div title="Code: 0xe9f3" class="large-16 medium-25 small-33">
				<i class="icon-right-big"></i>
				<span class="i-name">icon-right-big</span>
				<span class="i-code">0xe9f3</span>
			</div>
			<div title="Code: 0xe9f4" class="large-16 medium-25 small-33">
				<i class="icon-up-big"></i>
				<span class="i-name">icon-up-big</span>
				<span class="i-code">0xe9f4</span>
			</div>
			<div title="Code: 0xe9f9" class="large-16 medium-25 small-33">
				<i class="icon-left-circled-1"></i>
				<span class="i-name">icon-left-circled-1</span>
				<span class="i-code">0xe9f9</span>
			</div>
			<div title="Code: 0xe813" class="large-16 medium-25 small-33">
				<i class="icon-right-circled"></i>
				<span class="i-name">icon-right-circled</span>
				<span class="i-code">0xe813</span>
			</div>
			<div title="Code: 0xe814" class="large-16 medium-25 small-33">
				<i class="icon-up-circled"></i>
				<span class="i-name">icon-up-circled</span>
				<span class="i-code">0xe814</span>
			</div>
			<div title="Code: 0xe815" class="large-16 medium-25 small-33">
				<i class="icon-down-circled-2"></i>
				<span class="i-name">icon-down-circled-2</span>
				<span class="i-code">0xe815</span>
			</div>
			<div title="Code: 0xe816" class="large-16 medium-25 small-33">
				<i class="icon-cw"></i>
				<span class="i-name">icon-cw</span>
				<span class="i-code">0xe816</span>
			</div>
			<div title="Code: 0xe817" class="large-16 medium-25 small-33">
				<i class="icon-ccw"></i>
				<span class="i-name">icon-ccw</span>
				<span class="i-code">0xe817</span>
			</div>
			<div title="Code: 0xe9e8" class="large-16 medium-25 small-33">
				<i class="icon-arrows-cw"></i>
				<span class="i-name">icon-arrows-cw</span>
				<span class="i-code">0xe9e8</span>
			</div>
			<div title="Code: 0xe9ea" class="large-16 medium-25 small-33">
				<i class="icon-level-up"></i>
				<span class="i-name">icon-level-up</span>
				<span class="i-code">0xe9ea</span>
			</div>
			<div title="Code: 0xe9ec" class="large-16 medium-25 small-33">
				<i class="icon-level-down"></i>
				<span class="i-name">icon-level-down</span>
				<span class="i-code">0xe9ec</span>
			</div>
			<div title="Code: 0xe9fa" class="large-16 medium-25 small-33">
				<i class="icon-shuffle"></i>
				<span class="i-name">icon-shuffle</span>
				<span class="i-code">0xe9fa</span>
			</div>
			<div title="Code: 0xe9fb" class="large-16 medium-25 small-33">
				<i class="icon-exchange"></i>
				<span class="i-name">icon-exchange</span>
				<span class="i-code">0xe9fb</span>
			</div>
			<!-- Icon Temperature -->
			<h3>Temperature Icons</h3>
			<div title="Code: 0xe805" class="large-16 medium-25 small-33">
				<i class="icon-windy-rain-inv"></i>
				<span class="i-name">icon-windy-rain-inv</span>
				<span class="i-code">0xe805</span>
			</div>

			<div title="Code: 0xe82c" class="large-16 medium-25 small-33">
				<i class="icon-snow-inv"></i>
				<span class="i-name">icon-snow-inv</span>
				<span class="i-code">0xe82c</span>
			</div>
			<div title="Code: 0xe82d" class="large-16 medium-25 small-33">
				<i class="icon-snow-heavy-inv"></i>
				<span class="i-name">icon-snow-heavy-inv</span>
				<span class="i-code">0xe82d</span>
			</div>
			<div title="Code: 0xe82e" class="large-16 medium-25 small-33">
				<i class="icon-hail-inv"></i>
				<span class="i-name">icon-hail-inv</span>
				<span class="i-code">0xe82e</span>
			</div>
			<div title="Code: 0xe82f" class="large-16 medium-25 small-33">
				<i class="icon-clouds-inv"></i>
				<span class="i-name">icon-clouds-inv</span>
				<span class="i-code">0xe82f</span>
			</div>

			<div title="Code: 0xe830" class="large-16 medium-25 small-33">
				<i class="icon-clouds-flash-inv"></i>
				<span class="i-name">icon-clouds-flash-inv</span>
				<span class="i-code">0xe830</span>
			</div>
			<div title="Code: 0xe831" class="large-16 medium-25 small-33">
				<i class="icon-temperature"></i>
				<span class="i-name">icon-temperature</span>
				<span class="i-code">0xe831</span>
			</div>
			<div title="Code: 0xe8b9" class="large-16 medium-25 small-33">
				<i class="icon-compass"></i>
				<span class="i-name">icon-compass</span>
				<span class="i-code">0xe8b9</span>
			</div>
			<div title="Code: 0xe8ba" class="large-16 medium-25 small-33">
				<i class="icon-na"></i>
				<span class="i-name">icon-na</span>
				<span class="i-code">0xe8ba</span>
			</div>

			<div title="Code: 0xe8bb" class="large-16 medium-25 small-33">
				<i class="icon-celcius"></i>
				<span class="i-name">icon-celcius</span>
				<span class="i-code">0xe8bb</span>
			</div>
			<div title="Code: 0xe8bc" class="large-16 medium-25 small-33">
				<i class="icon-fahrenheit"></i>
				<span class="i-name">icon-fahrenheit</span>
				<span class="i-code">0xe8bc</span>
			</div>
			<div title="Code: 0xe8bd" class="large-16 medium-25 small-33">
				<i class="icon-clouds-flash-alt"></i>
				<span class="i-name">icon-clouds-flash-alt</span>
				<span class="i-code">0xe8bd</span>
			</div>
			<div title="Code: 0xe8be" class="large-16 medium-25 small-33">
				<i class="icon-sun-inv"></i>
				<span class="i-name">icon-sun-inv</span>
				<span class="i-code">0xe8be</span>
			</div>

			<div title="Code: 0xe8bf" class="large-16 medium-25 small-33">
				<i class="icon-moon-inv"></i>
				<span class="i-name">icon-moon-inv</span>
				<span class="i-code">0xe8bf</span>
			</div>
			<div title="Code: 0xe8c0" class="large-16 medium-25 small-33">
				<i class="icon-cloud-sun-inv"></i>
				<span class="i-name">icon-cloud-sun-inv</span>
				<span class="i-code">0xe8c0</span>
			</div>
			<div title="Code: 0xe8c1" class="large-16 medium-25 small-33">
				<i class="icon-cloud-moon-inv"></i>
				<span class="i-name">icon-cloud-moon-inv</span>
				<span class="i-code">0xe8c1</span>
			</div>
			<div title="Code: 0xe8c2" class="large-16 medium-25 small-33">
				<i class="icon-cloud-inv"></i>
				<span class="i-name">icon-cloud-inv</span>
				<span class="i-code">0xe8c2</span>
			</div>

			<div title="Code: 0xe8c3" class="large-16 medium-25 small-33">
				<i class="icon-cloud-flash-inv"></i>
				<span class="i-name">icon-cloud-flash-inv</span>
				<span class="i-code">0xe8c3</span>
			</div>
			<div title="Code: 0xe8c4" class="large-16 medium-25 small-33">
				<i class="icon-drizzle-inv"></i>
				<span class="i-name">icon-drizzle-inv</span>
				<span class="i-code">0xe8c4</span>
			</div>
			<div title="Code: 0xe8d1" class="large-16 medium-25 small-33">
				<i class="icon-rain-inv"></i>
				<span class="i-name">icon-rain-inv</span>
				<span class="i-code">0xe8d1</span>
			</div>
			<!-- Social Icon -->
			<h3>Social Icons</h3>
			<div title="Code: 0xe931" class="large-16 medium-25 small-33">
				<i class="icon-github"></i>
				<span class="i-name">icon-github</span>
				<span class="i-code">0xe931</span>
			</div>

			<div title="Code: 0xe932" class="large-16 medium-25 small-33">
				<i class="icon-github-circled"></i>
				<span class="i-name">icon-github-circled</span>
				<span class="i-code">0xe932</span>
			</div>
			<div title="Code: 0xe933" class="large-16 medium-25 small-33">
				<i class="icon-flickr"></i>
				<span class="i-name">icon-flickr</span>
				<span class="i-code">0xe933</span>
			</div>
			<div title="Code: 0xe934" class="large-16 medium-25 small-33">
				<i class="icon-flickr-circled"></i>
				<span class="i-name">icon-flickr-circled</span>
				<span class="i-code">0xe934</span>
			</div>
			<div title="Code: 0xe935" class="large-16 medium-25 small-33">
				<i class="icon-vimeo"></i>
				<span class="i-name">icon-vimeo</span>
				<span class="i-code">0xe935</span>
			</div>

			<div title="Code: 0xe936" class="large-16 medium-25 small-33">
				<i class="icon-vimeo-circled"></i>
				<span class="i-name">icon-vimeo-circled</span>
				<span class="i-code">0xe936</span>
			</div>
			<div title="Code: 0xe937" class="large-16 medium-25 small-33">
				<i class="icon-twitter"></i>
				<span class="i-name">icon-twitter</span>
				<span class="i-code">0xe937</span>
			</div>
			<div title="Code: 0xe938" class="large-16 medium-25 small-33">
				<i class="icon-twitter-circled"></i>
				<span class="i-name">icon-twitter-circled</span>
				<span class="i-code">0xe938</span>
			</div>
			<div title="Code: 0xe939" class="large-16 medium-25 small-33">
				<i class="icon-facebook"></i>
				<span class="i-name">icon-facebook</span>
				<span class="i-code">0xe939</span>
			</div>

			<div title="Code: 0xe93a" class="large-16 medium-25 small-33">
				<i class="icon-facebook-circled"></i>
				<span class="i-name">icon-facebook-circled</span>
				<span class="i-code">0xe93a</span>
			</div>
			<div title="Code: 0xe93b" class="large-16 medium-25 small-33">
				<i class="icon-facebook-squared"></i>
				<span class="i-name">icon-facebook-squared</span>
				<span class="i-code">0xe93b</span>
			</div>
			<div title="Code: 0xe93c" class="large-16 medium-25 small-33">
				<i class="icon-gplus"></i>
				<span class="i-name">icon-gplus</span>
				<span class="i-code">0xe93c</span>
			</div>
			<div title="Code: 0xe93d" class="large-16 medium-25 small-33">
				<i class="icon-gplus-circled"></i>
				<span class="i-name">icon-gplus-circled</span>
				<span class="i-code">0xe93d</span>
			</div>

			<div title="Code: 0xe93e" class="large-16 medium-25 small-33">
				<i class="icon-pinterest"></i>
				<span class="i-name">icon-pinterest</span>
				<span class="i-code">0xe93e</span>
			</div>
			<div title="Code: 0xe93f" class="large-16 medium-25 small-33">
				<i class="icon-pinterest-circled"></i>
				<span class="i-name">icon-pinterest-circled</span>
				<span class="i-code">0xe93f</span>
			</div>
			<div title="Code: 0xe940" class="large-16 medium-25 small-33">
				<i class="icon-tumblr"></i>
				<span class="i-name">icon-tumblr</span>
				<span class="i-code">0xe940</span>
			</div>
			<div title="Code: 0xe941" class="large-16 medium-25 small-33">
				<i class="icon-tumblr-circled"></i>
				<span class="i-name">icon-tumblr-circled</span>
				<span class="i-code">0xe941</span>
			</div>

			<div title="Code: 0xe942" class="large-16 medium-25 small-33">
				<i class="icon-linkedin"></i>
				<span class="i-name">icon-linkedin</span>
				<span class="i-code">0xe942</span>
			</div>
			<div title="Code: 0xe943" class="large-16 medium-25 small-33">
				<i class="icon-linkedin-circled"></i>
				<span class="i-name">icon-linkedin-circled</span>
				<span class="i-code">0xe943</span>
			</div>
			<div title="Code: 0xe944" class="large-16 medium-25 small-33">
				<i class="icon-dribbble"></i>
				<span class="i-name">icon-dribbble</span>
				<span class="i-code">0xe944</span>
			</div>
			<div title="Code: 0xe945" class="large-16 medium-25 small-33">
				<i class="icon-dribbble-circled"></i>
				<span class="i-name">icon-dribbble-circled</span>
				<span class="i-code">0xe945</span>
			</div>
			<div title="Code: 0xe946" class="large-16 medium-25 small-33">
				<i class="icon-stumbleupon"></i>
				<span class="i-name">icon-stumbleupon</span>
				<span class="i-code">0xe946</span>
			</div>
			<div title="Code: 0xe947" class="large-16 medium-25 small-33">
				<i class="icon-stumbleupon-circled"></i>
				<span class="i-name">icon-stumbleupon-circled</span>
				<span class="i-code">0xe947</span>
			</div>
			<div title="Code: 0xe949" class="large-16 medium-25 small-33">
				<i class="icon-lastfm-circled"></i>
				<span class="i-name">icon-lastfm-circled</span>
				<span class="i-code">0xe949</span>
			</div>
			<div title="Code: 0xe94a" class="large-16 medium-25 small-33">
				<i class="icon-rdio"></i>
				<span class="i-name">icon-rdio</span>
				<span class="i-code">0xe94a</span>
			</div>

			<div title="Code: 0xe94b" class="large-16 medium-25 small-33">
				<i class="icon-rdio-circled"></i>
				<span class="i-name">icon-rdio-circled</span>
				<span class="i-code">0xe94b</span>
			</div>
			<div title="Code: 0xe94c" class="large-16 medium-25 small-33">
				<i class="icon-spotify"></i>
				<span class="i-name">icon-spotify</span>
				<span class="i-code">0xe94c</span>
			</div>
			<div title="Code: 0xe94d" class="large-16 medium-25 small-33">
				<i class="icon-spotify-circled"></i>
				<span class="i-name">icon-spotify-circled</span>
				<span class="i-code">0xe94d</span>
			</div>
			<div title="Code: 0xe94e" class="large-16 medium-25 small-33">
				<i class="icon-qq"></i>
				<span class="i-name">icon-qq</span>
				<span class="i-code">0xe94e</span>
			</div>

			<div title="Code: 0xe94f" class="large-16 medium-25 small-33">
				<i class="icon-instagram"></i>
				<span class="i-name">icon-instagram</span>
				<span class="i-code">0xe94f</span>
			</div>
			<div title="Code: 0xe950" class="large-16 medium-25 small-33">
				<i class="icon-dropbox"></i>
				<span class="i-name">icon-dropbox</span>
				<span class="i-code">0xe950</span>
			</div>
			<div title="Code: 0xe951" class="large-16 medium-25 small-33">
				<i class="icon-evernote"></i>
				<span class="i-name">icon-evernote</span>
				<span class="i-code">0xe951</span>
			</div>
			<div title="Code: 0xe952" class="large-16 medium-25 small-33">
				<i class="icon-flattr"></i>
				<span class="i-name">icon-flattr</span>
				<span class="i-code">0xe952</span>
			</div>

			<div title="Code: 0xe953" class="large-16 medium-25 small-33">
				<i class="icon-skype"></i>
				<span class="i-name">icon-skype</span>
				<span class="i-code">0xe953</span>
			</div>
			<div title="Code: 0xe954" class="large-16 medium-25 small-33">
				<i class="icon-skype-circled"></i>
				<span class="i-name">icon-skype-circled</span>
				<span class="i-code">0xe954</span>
			</div>
			<div title="Code: 0xe955" class="large-16 medium-25 small-33">
				<i class="icon-renren"></i>
				<span class="i-name">icon-renren</span>
				<span class="i-code">0xe955</span>
			</div>
			<div title="Code: 0xe956" class="large-16 medium-25 small-33">
				<i class="icon-sina-weibo"></i>
				<span class="i-name">icon-sina-weibo</span>
				<span class="i-code">0xe956</span>
			</div>

			<div title="Code: 0xe957" class="large-16 medium-25 small-33">
				<i class="icon-paypal"></i>
				<span class="i-name">icon-paypal</span>
				<span class="i-code">0xe957</span>
			</div>
			<div title="Code: 0xe958" class="large-16 medium-25 small-33">
				<i class="icon-picasa"></i>
				<span class="i-name">icon-picasa</span>
				<span class="i-code">0xe958</span>
			</div>
			<div title="Code: 0xe959" class="large-16 medium-25 small-33">
				<i class="icon-soundcloud"></i>
				<span class="i-name">icon-soundcloud</span>
				<span class="i-code">0xe959</span>
			</div>
			<div title="Code: 0xe95a" class="large-16 medium-25 small-33">
				<i class="icon-mixi"></i>
				<span class="i-name">icon-mixi</span>
				<span class="i-code">0xe95a</span>
			</div>

			<div title="Code: 0xe95b" class="large-16 medium-25 small-33">
				<i class="icon-behance"></i>
				<span class="i-name">icon-behance</span>
				<span class="i-code">0xe95b</span>
			</div>
			<div title="Code: 0xe95c" class="large-16 medium-25 small-33">
				<i class="icon-google-circles"></i>
				<span class="i-name">icon-google-circles</span>
				<span class="i-code">0xe95c</span>
			</div>
			<div title="Code: 0xe95d" class="large-16 medium-25 small-33">
				<i class="icon-vkontakte"></i>
				<span class="i-name">icon-vkontakte</span>
				<span class="i-code">0xe95d</span>
			</div>
			<div title="Code: 0xe95e" class="large-16 medium-25 small-33">
				<i class="icon-smashing"></i>
				<span class="i-name">icon-smashing</span>
				<span class="i-code">0xe95e</span>
			</div>
		</div>
	</div>
</section>