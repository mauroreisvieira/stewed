<section id="link_button" data-spy="true" data-target="#link_button">
	<h1 class="header-line">Text Size</h1>
	<div class="row">
		<p class="fz-12">Lorem Ipsum</p>
		<p class="fz-16">Lorem Ipsum</p>
		<p class="fz-26">Lorem Ipsum</p>
		<p class="fz-36">Lorem Ipsum</p>
		<p class="fz-50">Lorem Ipsum</p>
		<p class="fz-60">Lorem Ipsum</p>
		<p class="fz-80">Lorem Ipsum</p>
		<p class="fz-90">Lorem Ipsum</p>
	</div>
</section>
<aside>
	<div id="view-html" class="code">
		<pre class="brush: xml; toolbar: false; gutter: false;">
		&lt;p class="fz-12">Lorem Ipsum</p>
		&lt;p class="fz-16">Lorem Ipsum</p>
		&lt;p class="fz-26">Lorem Ipsum</p>
		&lt;p class="fz-36">Lorem Ipsum</p>
		&lt;p class="fz-50">Lorem Ipsum</p>
		&lt;p class="fz-60">Lorem Ipsum</p>
		&lt;p class="fz-80">Lorem Ipsum</p>
		&lt;p class="fz-90">Lorem Ipsum</p>
		</pre>
	</div>
</aside>