<section id="link_button" data-spy="true" data-target="#link_button">
	<h1 class="header-line">Button</h1>
	<div class="row">
		<button class="btn stw-rect red ">btn stw-rect btn-red</button>
		<button class="btn stw-rect blue ">btn-blue</button>
		<button class="btn stw-rect green ">btn-green</button>
		<button class="btn stw-rect orange ">btn-orange</button>
		<button class="btn stw-rect yellow ">btn-yellow</button>
		<button class="btn stw-rect violet ">btn-violet </button>
		<button class="btn stw-rect dark-gray ">btn-dark-gray</button>
		<button class="btn stw-rect light-gray ">btn-light-gray</button>
	</div>
	<div class="space-20"></div>
	<div class="row">
		<button class="btn red ">btn btn-red</button>
		<button class="btn blue ">btn btn-blue</button>
		<button class="btn green ">btn btn-green</button>
		<button class="btn orange ">btn btn-orange</button>
		<button class="btn violet ">btn btn-violet</button>
		<button class="btn dark-gray ">btn btn-dark-gray</button>
		<button class="btn light-gray ">btn btn-light-gray</button>
	</div>
	<div class="space-20"></div>
	<div class="row">
		<button class="btn stw-btn red">btn stw-btn red</button>
		<button class="btn stw-btn blue">btn stw-btn blue</button>
		<button class="btn stw-btn green">btn stw-btn green</button>
		<button class="btn stw-btn orange">btn stw-btn orange</button>
		<button class="btn stw-btn violet">btn stw-btn violet</button>
		<button class="btn stw-btn dark-gray">btn stw-btn dark-gray</button>
	</div>
	<div class="space-20"></div>
	<div class="row">
		<button class="btn stw-round red ">1</button>
		<button class="btn stw-round blue ">2</button>
		<button class="btn stw-round green ">3</button>
		<button class="btn stw-round orange ">4</button>
		<button class="btn stw-round violet ">5</button>
		<button class="btn stw-round dark-gray ">+</button>
	</div>
</section>
<aside>
	<div id="view-html" class="code">
		<pre class="brush: xml; toolbar: false; gutter: false;">
		&lt;button class="btn stw-rect btn-dark-gray ">btn-dark-gray&lt;/button>
		&lt;button class="btn stw-rect btn-light-gray dark-gray">btn-light-gray&lt;/button>
		&lt;button class="btn btn-violet ">btn btn-violet&lt;/button>
		&lt;button class="btn btn-dark-gray ">btn btn-dark-gray&lt;/button>
		&lt;button class="btn stw-round btn-violet ">5&lt;/button>
		&lt;button class="btn stw-round btn-dark-gray ">+&lt;/button>
		</pre>
	</div>
</aside>