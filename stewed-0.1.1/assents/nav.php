<!-- nav menu -->
<section id="link-nav" data-spy="true" data-target="#link-nav">
	<h1 class="header-line">Navigation (Menu)</h1>
	<nav class="navigation">
		<ul class="menu horizontal black">
			<li class="active"><a href="#">Link</a>
			</li>
			<li><a href="#">Link</a>
			</li>
			<li><a href="#">Link</a>
			</li>
			<li><a href="#">Link</a>
			</li>
			<li><a href="#">Link</a>
			</li>
			<li class=""><a href="#">Link</a>
			</li>
			<li class="last-link"><a href="#">Link <i class="icon-down-dir-1"></i></a>
				<ul class="menu-down">
					<li><a href="#">Link</a>
					</li>
					<li><a href="#">Link</a>
					</li>
					<li><a href="#">Link</a>
					</li>
					<li><a href="#">Link</a>
					</li>
					<li><a href="#">Link</a>
					</li>
				</ul>
			</li>
		</ul>
	</nav>
	<div class="space-30"></div>
		<nav class="navigation medium ">
			<ul class="menu vertical red">
				<li class="active"><a href="#">Link</a>
				</li>
				<li><a href="#">Link</a>
				</li>
				<li><a href="#">Link</a>
				</li>
				<li><a href="#">Link <i class="icon-right-dir-1"></i></a>
					<ul class="menu-right">
						<li><a href="#">Link</a>
						</li>
						<li><a href="#">Link</a>
						</li>
						<li><a href="#">Link</a>
						</li>
					</ul>
				</li>
				<li class=""><a href="#">Link</a>
				</li>
				<li><a href="#">Link <i class="icon-down-dir-1"></i></a>
					<ul class="menu-down">
						<li><a href="#">Link</a>
						</li>
						<li><a href="#">Link</a>
						</li>
						<li><a href="#">Link</a>
						</li>
					</ul>
				</li>
				<li><a href="#">Link</a>
				</li>
			</ul>
		</nav>
	
</section>
<aside>
	<div id="view-html" class="code">
		<pre class="brush: xml; toolbar: false; gutter: false;">
			&lt;nav class="navigation medium ">
				&lt;ul class="menu vertical red">
					&lt;li class="active">&lt;a href="#"> Link &lt;/a>&lt;/li>
					&lt;!--  Menu Right -->
					&lt;li>&lt;a href="#"> Link &lt;/a>&lt;/li>&lt;li>
						&lt;a href="#">Link<i class="icon-right-dir-1"></i>&lt;/a>
						&lt;ul class="menu-right">
							&lt;li>&lt;a href="#"> Link &lt;/a>&lt;/li>
							&lt;li>&lt;a href="#"> Link &lt;/a>&lt;/li>
						&lt;/ul>
					&lt;/li>
					&lt;!--  Menu Down --> 
					&lt;li>
						&lt;a href="#">Link <i class="icon-down-dir-1"></i> &lt;/a>
						&lt;ul class="menu-down">
							&lt;li>&lt;a href="#"> Link &lt;/a>&lt;/li>
						&lt;/ul>
					&lt;/li>   
				&lt;/ul>
			&lt;/nav>
		</pre>
	</div>
</aside>