<!-- RESPONSIVE  -->
<section id="link_responsive" data-spy="true" data-target="#link_responsive">
	<div class="column-responsive">
		<h1 class="header-line">Grid to Responsive</h1>
		<div class="large-100 medium-100 small-100 column cor-orange">
			<p class="">large-100</p>
		</div>
		<div class="large-50 medium-50 small-100 column cor-orange">
			<p>large-50</p>
		</div>
		<div class="large-50 medium-50 small-100 column cor-violet">
			<p>large-50</p>
		</div>
		<div class="large-33 medium-100 small-100 column cor-orange">
			<p>large-33</p>
		</div>
		<div class="large-66 medium-100 small-100 column cor-violet">
			<p>large-66</p>
		</div>
		<div class="large-25 medium-100 small-100 column cor-orange">
			<p>large-25</p>
		</div>
		<div class="large-75 medium-100 small-100 column cor-violet">
			<p>large-75</p>
		</div>
		<div class="large-20 medium-100 small-100 column cor-orange">
			<p>large-20</p>
		</div>
		<div class="large-80 medium-100 small-100 column cor-violet">
			<p>large-80</p>
		</div>
		<div class="large-16 medium-50 small-100 column cor-orange">
			<p>large-16</p>
		</div>
		<div class="large-16 medium-50 small-100 column cor-violet">
			<p>large-16</p>
		</div>
		<div class="large-66 medium-100 small-100 column cor-green">
			<p>large-66</p>
		</div>
		<div class="large-12 medium-25 small-100 column cor-orange">
			<p>large-12</p>
		</div>
		<div class="large-12 medium-25 small-100 column cor-violet">
			<p>large-12</p>
		</div>
		<div class="large-75 medium-50 small-100 column cor-green">
			<p>large-75</p>
		</div>
		<div class="large-10 medium-20 small-100 column cor-orange">
			<p>large-10</p>
		</div>
		<div class="large-90 medium-80 small-100 column cor-violet">
			<p>large-90</p>
		</div>
		<div class="large-8 medium-25 small-100 column cor-orange">
			<p>large-8</p>
		</div>
		<div class="large-8 medium-25 small-100 column cor-violet">
			<p>large-8</p>
		</div>
		<div class="large-8 medium-25 small-100 column cor-red">
			<p>large-8</p>
		</div>
		<div class="large-8 medium-25 small-100 column cor-blue">
			<p>large-8</p>
		</div>
		<div class="large-66 medium-100 small-100 column cor-green">
			<p>large-66</p>
		</div>
	</div>
</section>
<aside>
	<div id="view-html" class="code">
		<pre class="brush: xml; toolbar: false; gutter: false;">
			&lt;div class="row">
				&lt;div class="large-100 medium-100 small-100">
					&lt;p class="">large-100 &lt;/p>
				&lt;/div>
				&lt;div class="large-50 medium-50 small-100">
					&lt;p>large-50 &lt;/p>
				&lt;/div>
				&lt;div class="large-50 medium-50 small-100">
					&lt;p>large-50 &lt;/p>
				&lt;/div>
			&lt;/div> 
		</pre>
	</div>
</aside>