<section id="link_button" data-spy="true" data-target="#link_button">
	<h1 class="header-line">Heading</h1>
	<div class="row">
		<h1>h1 heading</h1>
		<h2>h2 heading</h2>
		<h3>h3 heading</h3>
		<h4>h4 heading</h4>
		<h5>h5 heading</h5>
		<h6>h6 heading</h6>
	</div>
</section>
<aside>
	<div id="view-html" class="code">
		<pre class="brush: xml; toolbar: false; gutter: false;">
		&lt;h1>h1 heading&lt;/h1>
		&lt;h2>h2 heading&lt;/h2>
		&lt;h3>h3 heading&lt;/h3>
		&lt;h4>h4 heading&lt;/h4>
		&lt;h5>h5 heading&lt;/h5>
		&lt;h6>h6 heading&lt;/h6>
		</pre>
	</div>
</aside>