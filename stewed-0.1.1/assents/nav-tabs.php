<!-- nav tabs -->
<section id="link_tabs" data-spy="true" data-target="#link_tabs">
	<h1 class="header-line">Nav Tabs</h1>
	<div class="nav-tabs">
		<ul>
			<li class="tab tab-active" rel="nav-tab" data-target="#tab_1">First Tab</li>
			<li class="tab" rel="nav-tab" data-target="#tab_2">Second Tab</li>
			<li class="tab" rel="nav-tab" data-target="#tab_3">Third Tab</li>
		</ul>
		<div class="tab-container">
			<div class="tab-div" id="tab_1">
				<h5>First Tab Selected</h5>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis, quibusdam, hic, enim, repellendus voluptas illo quo alias vitae voluptatem quia accusantium ex iste excepturi accusamus unde atque maiores ducimus quaerat ut adipisci est eos veritatis nobis iusto nostrum earum possimus? </p>
			</div>
			<div class="tab-div" id="tab_2">
				<h5>Second Tab Selected</h5>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis, quibusdam, hic, enim, repellendus voluptas illo quo alias vitae voluptatem quia accusantium ex iste excepturi accusamus unde atque maiores ducimus quaerat ut adipisci est eos veritatis nobis iusto nostrum earum possimus? </p>
			</div>
			<div class="tab-div" id="tab_3">
				<h5>Third Tab Selected</h5>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis, quibusdam, hic, enim, repellendus voluptas illo quo alias vitae voluptatem quia accusantium ex iste excepturi accusamus unde atque maiores ducimus quaerat ut adipisci est eos veritatis nobis iusto nostrum earum possimus? </p>
			</div>
		</div>
	</div>
</section>
<aside>
	<div id="view-html" class="code">
		<pre class="brush: xml; toolbar: false; gutter: false;">
		&lt;div class="nav-tabs">
			&lt;ul>
				&lt;li class="tab tab-active" rel="nav-tab" data-target="#tab_1">First Tab&lt;/li>
				&lt;li class="tab" rel="nav-tab" data-target="#tab_2">Second Tab&lt;/li>
				&lt;li class="tab" rel="nav-tab" data-target="#tab_3">Third Tab&lt;/li>
			&lt;/ul>
			&lt;div class="tab-container">
				&lt;div class="tab-div" id="tab_1">
					&lt;h5>First Tab Selected&lt;/h5>
					&lt;p>Lorem ipsum dolor sit amet... &lt;/p>
				&lt;/div>
				&lt;div class="tab-div" id="tab_2">
					&lt;h5>Second Tab Selected&lt;/h5>
					&lt;p>Lorem ipsum dolor sit amet... &lt;/p>
				&lt;/div>
				&lt;div class="tab-div" id="tab_3">
					&lt;h5>Third Tab Selected&lt;/h5>
					&lt;p>Lorem ipsum dolor sit amet...&lt;/p>
				&lt;/div>
			&lt;/div>
		&lt;/div>
		</pre>
	</div>
</aside>