<!-- modal -->
<section id="link_modal" data-spy="true" data-target="#link_modal">
	<h1 class="header-line">Modal</h1>
	<a href="#janela" rel="modal">
		<button class="btn dark-gray">Exemple Modal</button>
	</a>
	<div class="modal-screen">
		<div class="modal " id="janela">
			<div class="modal-header">
				<a href="#" class="close-modal"><i class="icon-cancel"></i></a>
				<h4>Modal Screen</h4>
			</div>
			<div class="modal-container">
				<h5>Text Introduction Modal</h5>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam venenatis auctor tempus. Lorem ipsum dolor sit amet,</p>
				<p>Morbi dui lacus, placerat eget pretium vehicula, mollis id ligula. Nulla facilisi.</p>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore, natus reprehenderit sit esse fugiat explicabo assumenda repellendus deserunt. Asperiores, voluptatum, porro nostrum vitae quidem repellat amet voluptate fuga consequatur itaque laudantium cupiditate ratione eaque blanditiis aut pariatur libero molestias aperiam unde quasi. Ab, vitae, veniam inventore minus tempora fugiat quas sed quaerat quasi asperiores pariatur amet recusandae possimus ex odit minima quis illum temporibus aut ad eum totam explicabo hic nesciunt dolores commodi delectus veritatis itaque sapiente. Laboriosam, quasi, ducimus tempora iure reiciendis suscipit illum velit consequuntur voluptas impedit quibusdam et sit dignissimos ipsum ad officiis quidem iste atque perferendis vitae iusto ea voluptatum nisi molestias accusamus maxime molestiae. Perspiciatis, harum, dolor voluptatum voluptate quia perferendis doloribus. Saepe, esse, itaque!</p>
			</div>
			<div class="modal-footer">
				<button class="btn green">Save Changes</button>
				<a href="#" class="close-modal">
					<button class="btn red">Close</button>
				</a>
			</div>
		</div>
	</div>
</section>
<aside>
	<div id="view-html" class="code">
		<pre class="brush: xml; toolbar: false; gutter: false;">
			&lt;a href="#janela" rel="modal">
				&lt;button class="btn btn-default">Exemple Modal &lt;/button>
			&lt;/a>
			&lt;div class="modal-screen">
				&lt;div class="modal modal-round" id="janela">
					&lt;div class="modal-header">
						&lt;a href="#" class="close-modal"><i class="icon-cancel"></i> &lt;/a>
						&lt;h4>Modal Screen &lt;/h4>
					&lt;/div>
					&lt;div class="modal-container">
						&lt;h5>Text Introduction Modal &lt;/h5>
						&lt;p>Lorem ipsum dolor sit amet... &lt;/p>
					&lt;/div>
					&lt;div class="modal-footer">
						&lt;button class="btn-round btn-success">Save Changes &lt;/button>
						&lt;a href="#" class="close-modal">
							&lt;button class="btn-round btn-default">Close &lt;/button>
						&lt;/a>
					&lt;/div>
				&lt;/div>
			&lt;/div>
		</pre>
	</div>
</aside>