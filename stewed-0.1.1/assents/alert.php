<section id="link_alert" data-spy="true" data-target="#link_alert">
	<h1 class="header-line">Alert</h1>
	<div class="alert green">
		<p><strong>Success:</strong> Your account has been created!</p>
		<button class="close-alert"><i class="icon-cancel"></i></button>
	</div>
	<div class="alert blue">
		<p><strong>Information:</strong> Lorem ipsum dolor sit amet.</p>
		<button class="close-alert"><i class="icon-cancel"></i></button>
	</div>
	<div class="alert violet">
		<p><strong>Warning:</strong> Lorem ipsum dolor sit amet.</p>
		<button class="close-alert"><i class="icon-cancel"></i></button>
	</div>
	<div class="alert red">
		<p><strong>Danger:</strong> Lorem ipsum dolor sit amet.</p>
		<button class="close-alert"><i class="icon-cancel"></i></button>
	</div>
</section>
<aside>
	<div id="view-html" class="code">
		<pre class="brush: xml; toolbar: false; gutter: false;">
			&lt;div class="alert green">
				&lt;p>&lt;strong>Success:&lt;/strong> Your account has been created!&lt;/p>
				&lt;button class="close-alert">&lt;i class="icon-cancel">&lt;/i>&lt;/button>
			&lt;/div>
			&lt;div class="alert blue">
				&lt;p>&lt;strong>Information:&lt;/strong> Lorem ipsum dolor sit amet.&lt;/p>
				&lt;button class="close-alert">&lt;i class="icon-cancel">&lt;/i>&lt;/button>
			&lt;/div>
			&lt;div class="alert red">
				&lt;p>&lt;strong>Danger:&lt;/strong> Lorem ipsum dolor sit amet.&lt;/p>
				&lt;button class="close-alert">&lt;i class="icon-cancel">&lt;/i>&lt;/button>
			&lt;/div>
		</pre>
	</div>
</aside>