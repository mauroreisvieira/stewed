$(document).ready(function () {

    $('.backTop').click(function (e) {
        $("html, body").animate({
            scrollTop: 0
        }, 600);
        e.preventDefault();
    });

    $('.nav-button').click(function (e) {
        $(".nav-toggle").slideToggle("slow");
        $(".nav-line").addClass("mauro");
    });

    $('#nav-toggle').click(function (e) {
        this.classList.toggle("active");
    });

    $(window).scroll(function() {
        if ($(this).scrollTop() > 180) {
            $(".nav-fixed").css({'position':"fixed"});
            $(".nav-fixed").css({'top':"10px"});
        }
        if ($(this).scrollTop() < 230) {
            $(".nav-fixed").css({'position':"relative"});
            $(".nav-fixed").css({'top':"0px"});
        }
    });

    /* SLIDER */
    $(window).load(function(){
        $('.flexslider').flexslider({
            animation: "slide",
            start: function(slider){
                $('body').removeClass('loading');
            }
        });
    });

    /* MODAL */
    $("a[rel=modal]").click( function(ev){
        ev.preventDefault();
        var id = $(this).attr("href");
        $("body").css({'overflow':"hidden"});
        $('.modal-screen').fadeIn(300);
        $('.modal-screen').fadeTo("fast");
        $(id).show();  
    }); 

    $(".modal-screen").click( function(){
        $(this).hide();
        $("body").css({'overflow':"auto"});
    });

    $(".modal").click(function(){
        return false;
    });

    $('.close-modal').click(function(ev){
        ev.preventDefault();
        $(".modal-screen").hide();
        $(".modal").hide();
        $("body").css({'overflow':"auto"});
    });

    /* ALERTS */
    $(".close-alert").click(function() {
        $(this).parent("div").fadeToggle();
    });

    /* ACCORDION */
    $("a[rel=accordion]").click( function(ev){
        ev.preventDefault();
        var id = $(this).attr("href");
        $(id).slideToggle();  
    }); 

    /* NAV TABS */
    $("li[rel=nav-tab]").click( function(ev){
        ev.preventDefault();
        var id = $(this).attr("data-target");
        $(".tab-div").fadeOut(0, function () {
            $(id).show();
        });
    });

    $(".tab").click(function(){
        $('.tab').removeClass('tab-active');
        $(this).addClass('tab-active');
    }); 

});
