<?php 
if (isset($_GET['page'])) {
	$id = $_GET['page'];
}
else {
	$id = "home";
}
?>
<html>
	<head>
		<!-- Include Head -->
		<?php include 'layout/head.php'; ?>
	</head>
	<body>
		<header class="header">
			<!-- Include Header -->
			<?php include 'layout/header.php'; ?>
		</header>
		<div class="container">
			<!-- Include Pages -->
			<?php switch ($id) {
				case 'home':
				include ("home.php"); 
				break;
				case 'componentes':
				include ("components.php"); 
				break;
			}?>
		</div>
		<!-- Include Footer -->
		<?php include 'layout/footer.php'; ?>
	</body>
</html>