<div class="footer">
    <div class="grid-11">
        <div class="row">
            <p>©<?php echo date('Y');?> Stewed | Created with <i class="icon-heart"></i> by <a href="">Mauro Vieira</a> in Portugal</p>
        </div>
    </div>
</div>


<!-- Scripts -->
<script type="text/javascript " src="js/jquery-2.0.2.min.js "></script>
<script type="text/javascript " src="js/stewed.js"></script>
<script type="text/javascript " src="js/owl.carousel.js"></script>
<!-- Syntax Highlighter -->
<script type="text/javascript">
    $(function(){
        SyntaxHighlighter.all();
    });
</script>
<script type="text/javascript" src="assents/js/shCore.js"></script>
<script type="text/javascript" src="assents/js/shBrushXml.js"></script>
<script type="text/javascript" src="assents/js/shBrushJScript.js"></script>