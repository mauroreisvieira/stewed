<span class="space-20"></span>
<h5>Componentes</h5>
<div class="nav-fixed">
    <nav class="navigation">
        <ul class="menu vertical white">
            <li>
                <a href="?page=componentes&sub=global"<?php if($sub=='global'){ ?> class="activo" <?php } ?>>Global Typography</a>
            </li>
            <li><a href="?page=componentes&sub=icons" <?php if($sub=='icons'){ ?> class="activo" <?php } ?>>Graphic Icons</a>
                <ul>
                    <li><a href="" class="linkdisabled">Application Icons</a></li>
                    <li><a href="" class="linkdisabled">Control Icons</a></li>      
                    <li><a href="" class="linkdisabled">Text Editor Icons</a></li>           
                    <li><a href="" class="linkdisabled">Directional Icons</a></li>
                    <li><a href="" class="linkdisabled">Temperature Icons</a></li>
                    <li><a href="" class="linkdisabled">Social Icons</a></li>
                </ul>
            </li>
            <li>
                <a href="?page=componentes&sub=grid"<?php if($sub=='grid'){ ?> class="activo" <?php } ?>>Grid</a>
            </li>
            <li>
                <a href="?page=componentes&sub=buttons"<?php if($sub=='buttons'){ ?> class="activo" <?php } ?>>Button</a>
            </li>
            <li>
                <a href="?page=componentes&sub=pagination"<?php if($sub=='pagination'){ ?> class="activo" <?php } ?>>Pagination</a>
            </li>
            <li>
                <a href="?page=componentes&sub=navigation"<?php if($sub=='navigation'){ ?> class="activo" <?php } ?>>Navigation</a>
            </li>
            <li>
                <a href="?page=componentes&sub=alert"<?php if($sub=='alert'){ ?> class="activo" <?php } ?>>Alert</a>
            </li>
            <li>
                <a href="?page=componentes&sub=flexislider"<?php if($sub=='flexislider'){ ?> class="activo" <?php } ?>>Flexslider</a>
            </li>
            <li>
                <a href="?page=componentes&sub=modal"<?php if($sub=='modal'){ ?> class="activo" <?php } ?>>Modal</a>
            </li>
            <li>
                <a href="?page=componentes&sub=accordion"<?php if($sub=='accordion'){ ?> class="activo" <?php } ?>>Toggle Accordion</a>
            </li>
            <li>
                <a href="?page=componentes&sub=nav-tabs"<?php if($sub=='nav-tabs'){ ?> class="activo" <?php } ?>>Nav Tabs</a>
            </li> 
            <li>
                <a href="?page=componentes&sub=dropdown"<?php if($sub=='dropdown'){ ?> class="activo" <?php } ?>>Dropdown</a>
            </li>  
            <li>
                <a href="?page=componentes&sub=form"<?php if($sub=='form'){ ?> class="activo" <?php } ?>>Forms</a>
            </li>
        </ul>
    </nav>
</div>