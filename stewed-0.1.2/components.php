<?php 
if (isset($_GET['sub'])) {
    $sub = $_GET['sub'];
}
else {
    $sub = "global";
}
?>
<div class="row">
    <div class="intro">
        <div class="grid-11">
            <h2>Components UI</h2>
            <p>Stewed uses pixels and %</p>
            <p>Sans-serif is the default font family and the global size is 14 pixels.</p>
            <span class="space-30"></span>
        </div>
    </div>
</div>
<container class="container">
    <!-- Include Menu Fixed -->

    <div class="grid-12">
        <div class="row">
            <div class="large-3 medium-12">
                <?php include 'layout/menu-lateral.php'; ?>
            </div>
            <div class="large-9 medium-12">
                <?php switch ($sub) { 
                case 'icons':
                    include 'assents/icons.php';
                break;
                case 'grid':
                    include 'assents/grid.php';
                break;
                case 'accordion':
                    include 'assents/accordion.php';
                break;
                case 'buttons':
                    include 'assents/buttons.php';
                break;
                case 'alert':
                    include 'assents/alert.php';
                break;
                case 'dropdown':
                    include 'assents/dropdown.php';
                break;
                case 'flexislider':
                    include 'assents/flexislider.php';
                break;
                case 'global':
                    include 'assents/global.php';
                break;
                case 'modal':
                    include 'assents/modal.php';
                break;
                case 'pagination':
                    include 'assents/pagination.php';
                break;
                case 'nav-tabs':
                    include 'assents/nav-tabs.php';
                break;
                case 'navigation':
                    include 'assents/navigation.php';
                break;
                case 'alert':
                    include 'assents/alert.php';
                break;
                case 'form':
                    include 'assents/form.php';
                break;
                default:
                     include 'assents/global.php';
                break;
                } ?>
            </div>

        </div>
    </div>
</container>