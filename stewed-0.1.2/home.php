<!-- INTRO STEWD -->

<div class="intro">
    <img src="../stewed-0.1.1/assents/img/background.jpg" alt="">
</div>
<div class="intro-info">
    <h1> Stewed <span>| Cook Your Dinner! </span></h1>
    <p>This is a responsive framework: CSS, HTML & JavaScript.</p>
    <div class="space-30"></div>
    <button class="effect">Download</button>
    <button class="learn">Let's cook</button>
</div>

<div class="functions">
    <div class="grid-12">
        <div class="row">
            <div class="large-4"> 
                <div class="content-icons">
                    <i class="icon-cog-alt"></i> 
                </div>
                <div class="content-text">
                    <h4>Easy and Fast</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi possimus, sequi, eligendi, culpa quod provident vero dolorum tempora, dignissimos consequatur laudantium perferendis!</p>
                </div>
            </div>
            <div class="large-4"> 
                <div class="content-icons">
                    <i class="icon-folder"></i> 
                </div>
                <div class="content-text">
                    <h4>Organized</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi possimus, sequi, eligendi, culpa quod provident vero dolorum tempora, dignissimos consequatur laudantium perferendis!</p>
                </div>
            </div>
            <div class="large-4"> 
                <div class="content-icons">
                    <i class="icon-code-1"></i> 
                </div>
                <div class="content-text">
                    <h4>Simple Code</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi possimus, sequi, eligendi, culpa quod provident vero dolorum tempora, dignissimos consequatur laudantium perferendis!</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="description-stewed">
    <div class="grid-11">

    </div>
</div>