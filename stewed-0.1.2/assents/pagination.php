<section id="link_pagination" data-spy="true" data-target="#link_pagination">
	<h1 class="header-line">Pagination</h1>
	<div class="row">
		<ul class="pagination">
			<li><a href="#"><i class="icon-left-open-big"></i></a></li>
			<li><a href="#" class="active">1</a></li>
			<li><a href="#">2</a></li>
			<li><a href="#">3</a></li>
			<li><a href="#">4</a></li>
			<li><a href="#">5</a></li>
			<li><a href="#">...</a></li>
			<li><a href="#">15</a></li>
			<li><a href="#"><i class="icon-right-open-big"></i></a></li>
		</ul>
	</div>
</section>
<aside>
	<div id="view-html" class="code">
		<pre class="brush: xml; toolbar: false; gutter: false;">
		 &lt;ul class="pagination">
			 &lt;li>&lt;a href="#">&lt;i class="icon-left-open-big">&lt;/i>&lt;/a>&lt;/li>
			 &lt;li>&lt;a href="#" class="active">1&lt;/a>&lt;/li>
			 &lt;li>&lt;a href="#">2&lt;/a>&lt;/li>
			 &lt;li>&lt;a href="#">3&lt;/a>&lt;/li>
			 &lt;li>&lt;a href="#">4&lt;/a>&lt;/li>
			 &lt;li>&lt;a href="#">5&lt;/a>&lt;/li>
			 &lt;li>&lt;a href="#">&lt;i class="icon-right-open-big">&lt;/i>&lt;/a>&lt;/li>
		 &lt;/ul>
		</pre>
	</div>
</aside>