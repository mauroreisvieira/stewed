<section id="link_responsive" data-spy="true" data-target="#link_responsive">
    <div class="column-responsive">
        <h1 class="header-line">Column Grid</h1>
        <!-- LARGE 1 -->
        <div class="row no-space">
            <div class="large-1 column">1</div>
            <div class="large-1 column">1</div>
            <div class="large-1 column">1</div>
            <div class="large-1 column">1</div>
            <div class="large-1 column">1</div>
            <div class="large-1 column">1</div>
            <div class="large-1 column">1</div>
            <div class="large-1 column">1</div>
            <div class="large-1 column">1</div>
            <div class="large-1 column">1</div>
            <div class="large-1 column">1</div>
            <div class="large-1 column">1</div>
        </div>
        <!-- LARGE 2 -->
        <div class="row no-space">
            <div class="large-2 column">2</div>
            <div class="large-2 column">2</div>
            <div class="large-2 column">2</div>
            <div class="large-2 column">2</div>
            <div class="large-2 column">2</div>
            <div class="large-2 column">2</div>
        </div>
        <!-- LARGE 3 -->
        <div class="row no-space">
            <div class="large-3 column">3</div>
            <div class="large-3 column">3</div>
            <div class="large-3 column">3</div>
            <div class="large-3 column">3</div>
        </div>
        <!-- LARGE 4 -->
        <div class="row no-space">
            <div class="large-4 column">4</div>
            <div class="large-4 column">4</div>
            <div class="large-4 column">4</div>
        </div>
        <!-- LARGE 5 + 7 -->
        <div class="row no-space">
            <div class="large-5 column">5</div>
            <div class="large-7 column">7</div>
        </div>
        <!-- LARGE 6 -->
        <div class="row no-space">
            <div class="large-6 column">6</div>
            <div class="large-6 column">6</div>
        </div>
        <!-- LARGE 7 + 3 + 2 -->
        <div class="row no-space">
            <div class="large-7 column">7</div>
            <div class="large-3 column">3</div>
            <div class="large-2 column">2</div>
        </div>
        <!-- LARGE 8 + 4 -->
        <div class="row no-space">
            <div class="large-8 column">8</div>
            <div class="large-4 column">4</div>
        </div>
        <!-- LARGE 9 + 3 -->
        <div class="row no-space">
            <div class="large-9 column">9</div>
            <div class="large-3 column">3</div>
        </div>
        <!-- LARGE 10 + 2 -->
        <div class="row no-space">
            <div class="large-10 column">10</div>
            <div class="large-2 column">2</div>
        </div>
        <!-- LARGE 11 + 1 -->
        <div class="row no-space">
            <div class="large-11 column">11</div>
            <div class="large-1 column">1</div>
        </div>
        <!-- LARGE 12 -->
        <div class="row no-space">
            <div class="large-12 column">12</div>
        </div>
    </div>
</section>
<aside>
    <div class="uppercase ">
        <p class="fz-14 bold color-1">Cooking</p>
    </div>
    <div id="view-html" class="code">
        <pre class="brush: xml; toolbar: false; gutter: false;">
            &lt;div class="row">
                &lt;div class="large-4 column">4&lt;/div>
                &lt;div class="large-4 column">4&lt;/div>
                &lt;div class="large-4 column">4&lt;/div>
            &lt;/div>

            &lt;div class="row">
                &lt;div class="large-4 column">4&lt;/div>
                &lt;div class="large-8 column">8&lt;/div>
            &lt;/div>

            &lt;div class="row">
                &lt;div class="large-6 column">6&lt;/div>
                &lt;div class="large-6 column">6&lt;/div>
            &lt;/div>

            &lt;div class="row">
                &lt;div class="large-12 column">12&lt;/div>
            &lt;/div>
		</pre>
    </div>
</aside>


<section id="link_responsive" data-spy="true" data-target="#link_responsive">
    <div class="column-responsive">
        <h1 class="header-line">Column Grid</h1>
        <!-- LARGE 1 -->
        <div class="row-space">
            <div class="large-1 column">1</div>
            <div class="large-1 column">1</div>
            <div class="large-1 column">1</div>
            <div class="large-1 column">1</div>
            <div class="large-1 column">1</div>
            <div class="large-1 column">1</div>
            <div class="large-1 column">1</div>
            <div class="large-1 column">1</div>
            <div class="large-1 column">1</div>
            <div class="large-1 column">1</div>
            <div class="large-1 column">1</div>
            <div class="large-1 column">1</div>
        </div>
        <!-- LARGE 2 -->
        <div class="row-space">
            <div class="large-2 column">2</div>
            <div class="large-2 column">2</div>
            <div class="large-2 column">2</div>
            <div class="large-2 column">2</div>
            <div class="large-2 column">2</div>
            <div class="large-2 column">2</div>
        </div>
        <!-- LARGE 3 -->
        <div class="row-space">
            <div class="large-3 column">3</div>
            <div class="large-3 column">3</div>
            <div class="large-3 column">3</div>
            <div class="large-3 column">3</div>
        </div>
        <!-- LARGE 4 -->
        <div class="row-space">
            <div class="large-4 column">4</div>
            <div class="large-4 column">4</div>
            <div class="large-4 column">4</div>
        </div>
        <!-- LARGE 5 + 7 -->
        <div class="row-space">
            <div class="large-5 column">5</div>
            <div class="large-7 column">7</div>
        </div>
        <!-- LARGE 6 -->
        <div class="row-space">
            <div class="large-6 column">6</div>
            <div class="large-6 column">6</div>
        </div>
        <!-- LARGE 7 + 5 -->
        <div class="row-space">
            <div class="large-7 column">7</div>
            <div class="large-5 column">5</div>
        </div>
        <!-- LARGE 8 + 4 -->
        <div class="row-space">
            <div class="large-8 column">8</div>
            <div class="large-4 column">4</div>
        </div>
        <!-- LARGE 9 + 3 -->
        <div class="row-space">
            <div class="large-9 column">9</div>
            <div class="large-3 column">3</div>
        </div>
        <!-- LARGE 10 + 2 -->
        <div class="row-space">
            <div class="large-10 column">10</div>
            <div class="large-2 column">2</div>
        </div>
        <!-- LARGE 11 + 1 -->
        <div class="row-space">
            <div class="large-11 column">11</div>
            <div class="large-1 column">1</div>
        </div>
        <!-- LARGE 12 -->
        <div class="row-space">
            <div class="large-12 column">12</div>
        </div>
    </div>
</section>
<aside>
    <div class="uppercase ">
        <p class="fz-14 bold color-1">Cooking</p>
    </div>
    <div id="view-html" class="code">
        <pre class="brush: xml; toolbar: false; gutter: false;">
        &lt;div class="row-space">
            &lt;div class="large-4 column">4&lt;/div>
            &lt;div class="large-4 column">4&lt;/div>
            &lt;div class="large-4 column">4&lt;/div>
        &lt;/div>

        &lt;div class="row-space">
            &lt;div class="large-4 column">4&lt;/div>
            &lt;div class="large-8 column">8&lt;/div>
        &lt;/div>

        &lt;div class="row-space">
            &lt;div class="large-6 column">6&lt;/div>
            &lt;div class="large-6 column">6&lt;/div>
        &lt;/div>

        &lt;div class="row-space">
            &lt;div class="large-12 column">12&lt;/div>
        &lt;/div>
		</pre>
    </div>
</aside>