
<section id="link_modal" data-spy="true" data-target="#link_modal">
    <h1 class="header-line">Dropdown Hover</h1>
    <nav>
        <div class="dropdown hover">
            <a href="#">Hover Menu <i class="icon-cog-1"></i></a>
            <ul>
                <li><a href="#">Item</a></li>
                <li><a href="#">Product</a></li>
                <li><a href="#">Text</a></li>
                <li><a href="#">Page</a></li>
                <li><a href="#">Thing</a></li>
                <li><a href="#">Product</a></li>
                <li><a href="#">Text</a></li>
            </ul>
        </div>
    </nav>
</section>
<aside>
    <div class="uppercase ">
        <p class="fz-14 bold color-1">Cooking</p>
    </div>
    <div id="view-html" class="code">
        <pre class="brush: xml; toolbar: false; gutter: false;">
                &lt;div class="nav-tabs">
                    &lt;ul>
                        &lt;li class="tab tab-active" rel="nav-tab" data-target="#tab_1">First Tab&lt;/li>
                        &lt;li class="tab" rel="nav-tab" data-target="#tab_2">Second Tab&lt;/li>
                        &lt;li class="tab" rel="nav-tab" data-target="#tab_3">Third Tab&lt;/li>
                    &lt;/ul>
                    &lt;div class="tab-container">
                        &lt;div class="tab-div" id="tab_1">
                            &lt;h5>First Tab Selected&lt;/h5>
                            &lt;p>Lorem ipsum dolor sit amet... &lt;/p>
                        &lt;/div>
                        &lt;div class="tab-div" id="tab_2">
                            &lt;h5>Second Tab Selected&lt;/h5>
                            &lt;p>Lorem ipsum dolor sit amet... &lt;/p>
                        &lt;/div>
                        &lt;div class="tab-div" id="tab_3">
                            &lt;h5>Third Tab Selected&lt;/h5>
                            &lt;p>Lorem ipsum dolor sit amet...&lt;/p>
                        &lt;/div>
                    &lt;/div>
                &lt;/div>
                </pre>
    </div>
</aside>


<section id="link_modal" data-spy="true" data-target="#link_modal">
    <h1 class="header-line">Dropdown Click</h1>

    <nav>
        <div class="dropdown toggle">
            <input id="t1" type="checkbox">
            <label for="t1">Toggle Menu <i class="icon-cog-1"></i></label>
            <ul>
                <li><a href="#">Item</a></li>
                <li><a href="#">Product</a></li>
                <li><a href="#">Text</a></li>
                <li><a href="#">Page</a></li>
                <li><a href="#">Thing</a></li>
            </ul>
        </div>
    </nav>
</section>
<aside>
    <div class="uppercase ">
        <p class="fz-14 bold color-1">Cooking</p>
    </div>
    <div id="view-html" class="code">
        <pre class="brush: xml; toolbar: false; gutter: false;">
		&lt;div class="nav-tabs">
			&lt;ul>
				&lt;li class="tab tab-active" rel="nav-tab" data-target="#tab_1">First Tab&lt;/li>
				&lt;li class="tab" rel="nav-tab" data-target="#tab_2">Second Tab&lt;/li>
				&lt;li class="tab" rel="nav-tab" data-target="#tab_3">Third Tab&lt;/li>
			&lt;/ul>
			&lt;div class="tab-container">
				&lt;div class="tab-div" id="tab_1">
					&lt;h5>First Tab Selected&lt;/h5>
					&lt;p>Lorem ipsum dolor sit amet... &lt;/p>
				&lt;/div>
				&lt;div class="tab-div" id="tab_2">
					&lt;h5>Second Tab Selected&lt;/h5>
					&lt;p>Lorem ipsum dolor sit amet... &lt;/p>
				&lt;/div>
				&lt;div class="tab-div" id="tab_3">
					&lt;h5>Third Tab Selected&lt;/h5>
					&lt;p>Lorem ipsum dolor sit amet...&lt;/p>
				&lt;/div>
			&lt;/div>
		&lt;/div>
		</pre>
    </div>
</aside>

