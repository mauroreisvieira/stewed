<section id="link_button" data-spy="true" data-target="#link_button">
    <h1 class="header-line">Forms</h1>
    <div class="large-12">
        <form class="form m">
            <div class="fieldset">
                <label for="name"><i class="icon-user-1"></i></label>
                <input id="name" name="name" value="" type="text" placeholder="Name" required>
            </div>
            <div class="fieldset">
                <label for="email"><i class="icon-mail"></i></label>
                <input id="email" name="email" value="" type="email" placeholder="E-mail" required>
            </div> 

            <div class="fieldset">
                <label for="tel"><i class="icon-phone-1"></i></label>
                <input id="" name="" value="" type="tel" placeholder="Phone" required>
            </div>

            <div class="fieldset">
                <label for="password"><i class="icon-key"></i></label>
                <input id="password" type="password" value="" placeholder="Password" required>
            </div>
            <div class="fieldset">
                <div class="select">
                    <select name="one" class="dropdown-select">
                        <option value="">Custom Select Option </option>
                        <option value="1">Option #1</option>
                        <option value="2">Option #2</option>
                        <option value="3">Option #3</option>
                    </select>
                </div>
            </div>

            <div class="fieldset">
                <label for="password"><i class="icon-pencil-1"></i></label>
                <textarea id="textarea" required></textarea>
            </div>

            <div class="fieldset">
                <span>Remember me</span>
                <input type="checkbox" name="choice2" class="error"/>
            </div>

            <div class="fieldset">
                <input id="create" type="submit" value="Create Account">
                <input id="reset" type="reset" value="Clean">
            </div>
        </form>
    </div> 

</section>

<aside>
    <div class="uppercase ">
        <p class="fz-14 bold color-1">Cooking</p>
    </div>
    <div id="view-html" class="code">
        <pre class="brush: xml; toolbar: false; gutter: false;">
        &lt;form class="form m">
            &lt;div class="fieldset">
                &lt;label for="name">&lt;i class="icon-user-1">&lt;/i>&lt;/label>
                &lt;input id="name" name="name" value="" type="text" placeholder="Name" required>
            &lt;/div>
            &lt;div class="fieldset">
                &lt;label for="email">&lt;i class="icon-mail">&lt;/i>&lt;/label>
                &lt;input id="email" name="email" value="" type="email" placeholder="E-mail" required>
            &lt;/div> 

            &lt;div class="fieldset">
                &lt;label for="tel">&lt;i class="icon-phone-1">&lt;/i>&lt;/label>
                &lt;input id="" name="" value="" type="tel" placeholder="Phone" required>
            &lt;/div>

            &lt;div class="fieldset">
                &lt;label for="password">&lt;i class="icon-key">&lt;/i>&lt;/label>
                &lt;input id="password" type="password" value="" placeholder="Password" required>
            &lt;/div>
       
            &lt;div class="fieldset">
                &lt;div class="select">
                    &lt;select name="one" class="dropdown-select">
                        &lt;option value="">Custom Select Option &lt;/option>
                        &lt;option value="1">Option 1&lt;/option>
                        &lt;option value="2">Option 2&lt;/option>
                        &lt;option value="3">Option 3&lt;/option>
                    &lt;/select>
                &lt;/div>
            &lt;/div>

            &lt;div class="fieldset">
                &lt;label for="password">&lt;i class="icon-pencil-1">&lt;/i>&lt;/label>
                &lt;textarea id="textarea" required>&lt;/textarea>
            &lt;/div>

            &lt;div class="fieldset">
                &lt;span>Remember me&lt;/span>
                &lt;input type="checkbox" name="choice2" class="error"/>
            &lt;/div>

            &lt;div class="fieldset">
                &lt;input id="create" type="submit" value="Create Account">
                &lt;input id="reset" type="reset" value="Clean">
            &lt;/div>
        
        &lt;/form>
        </pre>
    </div>
</aside>

