$(document).ready(function () {
    $('a[href^="#"]').on('click', function(event) {
        var target = $( $(this).attr('href') );
        if( target.length ) {
            event.preventDefault();
            $('html, body').animate({
                scrollTop: target.offset().top
            }, 1000);
        }
    });

    $('.nav-button').click(function (e) {
        $(".nav-toggle").slideToggle("slow");
        $(".nav-line").addClass("mauro");
    });

    $('#nav-toggle').click(function (e) {
        this.classList.toggle("active");
    });

    /* MODAL */
    $("a[rel=modal]").click( function(ev){
        ev.preventDefault();
        var id = $(this).attr("href");
        $("body").css({'overflow':"hidden"});
        $('.modal-screen').fadeIn(300);
        $('.modal-screen').fadeTo("fast");
        $(id).show();  
    }); 

    $(".modal-screen").click( function(){
        $(this).hide();
        $("body").css({'overflow':"auto"});
    });

    $(".modal").click(function(){
        return false;
    });

    $('.close-modal').click(function(ev){
        ev.preventDefault();
        $(".modal-screen").hide();
        $(".modal").hide();
        $("body").css({'overflow':"auto"});
    });

    /* ALERTS */
    $(".close-alert").click(function() {
        $(this).parent("div").fadeToggle();
    });

    /* ACCORDION */
    $("a[rel=accordion]").click( function(ev){
        ev.preventDefault();
        var id = $(this).attr("href");
        var idxx = $(this).attr("data-target");
        var idx = document.getElementById (idxx);

        if ($(idx).hasClass("tog-off")) {
            $(idx).removeClass("tog-off");
            $(idx).addClass("tog-on");
            $(id).slideToggle();  
        }else {
            $(idx).removeClass("tog-on");
            $(idx).addClass("tog-off");
            $(id).slideToggle();  
        }
    }); 

    /* NAV TABS */
    $("li[rel=nav-tab]").click( function(ev){
        ev.preventDefault();
        var id = $(this).attr("data-target");
        $(".tab-div").fadeOut(0, function () {
            $(id).show();
        });
    });

    $(".tab").click(function(){
        $('.tab').removeClass('tab-active');
        $(this).addClass('tab-active');
    }); 

    /* OWL */
    $("#owl-demo").owlCarousel({
        autoPlay : 3000,
        stopOnHover : true,
        navigation:true,
        paginationSpeed : 1000,
        goToFirstSpeed : 2000,
        singleItem : true,
        autoHeight : true,
        transitionStyle:"fade"
    });

    /* DROP DOWN */

    // close the toggle menu if user clicks outside of the menu

    $(document).click(function(event) {
        if(
            $('.toggle > input').is(':checked') &&
            !$(event.target).parents('.toggle').is('.toggle')
        ) {
            $('.toggle > input').prop('checked', false);
        }
    })


});
